package com.example.faytxzen.simpledictionary;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by faytxzen on 3/11/15.
 */
public class DictionaryHelper extends SQLiteOpenHelper {
    /* DATA MEMBERS
     *------------------------*/
    public static final boolean debug = DictionaryFragment.debug;
    public static final String
            DATABASE_NAME = "simpleDictionary",
            DICTIONARY_TABLE_NAME = "dictionary",
            KEY_TERM = "term",
            KEY_DEFINITION = "definition";
    public static final int DATABASE_VERSION = 1;

    public DictionaryHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String DICTIONARY_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + DICTIONARY_TABLE_NAME + " (" + KEY_TERM + " TEXT, " + KEY_DEFINITION + " TEXT);";
    private static final String DICTIONARY_TABLE_UPGRADE = "DROP TABLE IF EXISTS " + DICTIONARY_TABLE_NAME;

    /* DATABASE LIFECYCLE METHODS
     *------------------------*/
    @Override
    public void onCreate( SQLiteDatabase db ){
        if(debug) Log.d("DEBUG", "public void onCreate( SQLiteDatabase db )");
        db.execSQL( DICTIONARY_TABLE_CREATE );
    }

    @Override
    public void onUpgrade( SQLiteDatabase db, int oldVer, int newVer ){
        db.execSQL(DICTIONARY_TABLE_UPGRADE);
        db.execSQL(DICTIONARY_TABLE_CREATE);
    }
}
