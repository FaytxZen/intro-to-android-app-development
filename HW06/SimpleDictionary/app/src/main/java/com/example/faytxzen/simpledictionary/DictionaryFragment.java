package com.example.faytxzen.simpledictionary;

import android.app.Activity;
import android.app.Fragment;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by faytxzen on 3/12/15.
 */
public class DictionaryFragment extends Fragment {

    /**
     * Inner interface for the DictionaryTask and classes employing it
     */
    static interface TaskCallbackTarget {
        void onPreExecute();
        void onProgressUpdate(String... values);
        void onCancelled();
        void onPostExecute();
    }

    /* DATA MEMBERS
     *------------------------*/
    private TaskCallbackTarget mTarget;
    private DictionaryDAO dicDao;
    public static final boolean debug = false;

    /* LIFECYCLE METHODS
     *------------------------*/
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if(debug) Log.d("DEBUG", "public void onAttach(Activity activity)");

        mTarget = (TaskCallbackTarget)activity;

        if(mTarget != null && dicDao == null){
            if(debug) Log.d("DEBUG", "onAttach(Activity activity): if(mTarget != null && dicDao == null)");
            dicDao = new DictionaryDAO(activity.getApplicationContext());
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if(debug) Log.d("DEBUG", "public void onDetach()");

        mTarget = null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    /* OTHER METHODS
     *------------------------*/
    public void createTask(String task, String term, String definition){
        if(debug) Log.d("DEBUG", "public void createTask(...) : " + (dicDao == null));
        DictionaryTask dt = new DictionaryTask(dicDao, task, term, definition);
        dt.execute();
    }

    /* INNER CLASSES
     *------------------------*/
    /**
     * Private inner class for any database tasks
     */
    private class DictionaryTask extends AsyncTask<Void, String, Void> {
        private String task, term, definition;
        private DictionaryDAO dd;

        public DictionaryTask(DictionaryDAO dd, String task, String term, String definition){
            this.dd = dd;
            this.task = task;
            this.term = term;
            this.definition = definition;
        }

        /** Interface methods
         *--------------------------------------*/
        @Override
        protected Void doInBackground(Void... params) {
            switch( task ){
                case "find":
                    findEntry();
                    break;

                case "update":
                    updateEntry();
                    break;

                case "delete":
                    deleteEntry();
                    break;

                case "reboot":
                    rebootDb();
                    break;
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            if( mTarget!=null ) mTarget.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if( mTarget!=null ) mTarget.onPostExecute();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            if( mTarget!=null ) mTarget.onProgressUpdate(values[0], values[1]);
        }

        @Override
        protected void onCancelled() {
            if( mTarget!=null ) mTarget.onCancelled();
        }

        /** Database Methods
         *---------------------------------------*/
        /**
         * Find the term given in the constructor
         */
        private void findEntry() {
            if(debug) Log.d("DEBUG", "private void findEntry()");
            if(debug) Log.d("DEBUG", "findEntry: dd == null : "+ (dd == null));

            Cursor results = dd.findDictionaryEntry(term);
            results.moveToFirst();

            if (results.getCount() > 0) {
                publishProgress(term, results.getString(1));
            } else {
                publishProgress(term, definition);
                makeToast("Definition not found. Please update it.");
            }
            results.close();
        }

        /**
         * Update the term in the database with the new definition from the constructor
         * If the term doesn't exist, it will be inserted
         */
        private void updateEntry(){
            if(debug) Log.d("DEBUG", "private void updateEntry()");

            publishProgress(term, definition);

            if(debug) Log.d("DEBUG", "updateEntry - publishProgress(term, definition)");
            if(debug) Log.d("DEBUG", "updateEntry - dd == null : "+ (dd == null));

            boolean result = dd.updateDictionaryEntry(term, definition);

            if(debug) Log.d("DEBUG", "result = "+result);

            makeToast(result ? "Definition updated!" : "Update failed. Please try again.");
        }

        /**
         * Delete all entries with the term given in the constructor
         */
        private void deleteEntry(){
            if(debug) Log.d("DEBUG", "private void deleteEntry()");

            publishProgress(term, definition);
            final boolean result = dd.deleteDictionaryEntry(term);

            makeToast(result ? "Term deleted!" : "Delete failed. Please try again.");
        }

        private void rebootDb(){
            boolean result = dd.dropAndCreateDb();
            makeToast(result ? "Database dropped and created!" : "Failed to reboot database. Try again.");
        }

        private void makeToast(final CharSequence txt){
            if(mTarget != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ((MainActivity)mTarget).makeToast(txt);
                    }
                });
            }
        }
    }//end-DictionaryTask
}
