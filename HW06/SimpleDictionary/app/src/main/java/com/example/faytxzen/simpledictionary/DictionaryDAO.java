package com.example.faytxzen.simpledictionary;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by faytxzen on 3/11/15.
 */
public class DictionaryDAO {
    /* DATA MEMBERS
     *---------------------------------*/
    public static final boolean debug = DictionaryFragment.debug;
    private DictionaryHelper helper;
    private SQLiteDatabase database;

    private static String   DICT_TABLE = DictionaryHelper.DICTIONARY_TABLE_NAME,
                            DICT_TERM = DictionaryHelper.KEY_TERM,
                            DICT_DEF = DictionaryHelper.KEY_DEFINITION;

    public DictionaryDAO(Context context){
        if(debug) Log.d("DEBUG", "public DictionaryDAO(Context context)");

        helper = new DictionaryHelper(context);
        database = helper.getWritableDatabase();
    }

    /* DATABASE METHODS
     *--------------------------------*/
    /**
     * Delete all instances of the given term from the database
     * @param term
     * @return
     */
    public boolean deleteDictionaryEntry(String term){
        if(debug) Log.d("DEBUG", "public boolean deleteDictionaryEntry(String term)");

        int rowsDeleted = database.delete(DICT_TABLE, "? = ?", new String[] { DICT_TERM, term });
        if(rowsDeleted > 0) return true;
        return false;
    }

    /**
     * Add the entry to to the database
     * @param term the term to add
     * @param def the definition to accompany the term
     * @return
     */
    public boolean addDictionaryEntry(String term, String def){
        if(debug) Log.d("DEBUG", "public boolean addDictionaryEntry(String term, String def)");

        ContentValues values = new ContentValues();

        values.put(DICT_TERM, term);
        values.put(DICT_DEF, def);

        long result = database.insert(DICT_TABLE, null, values);

        if(result == -1) return false;
        return true;
    }

    /**
     * Update the given term with the new given definition
     * It inserts the term and definition if it doesn't exist
     * @param term the term to update
     * @param def the definition to update the term with
     * @return
     */
    public boolean updateDictionaryEntry(String term, String def){
        if(debug) Log.d("DEBUG", "public boolean updateDictionaryEntry(String term, String def)");

        Cursor check = findDictionaryEntry(term);

        if(check.getCount() < 1) {
            check.close();
            return addDictionaryEntry(term, def);
        }

        check.close(); //close the cursor

        //Create the ContentValues instance and add the values
        ContentValues values = new ContentValues();
        values.put(DICT_DEF, def);

        //Query the database to update the term with the new definition
        int result = database.update(DICT_TABLE, values, DICT_TERM + " = ?", new String[] { term });

        //If it effectively changes the db, return true
        if(result > 0) return true;
        return false;
    }

    /**
     * Find the term and definition of the given term
     * @param term
     * @return
     */
    public Cursor findDictionaryEntry(String term){
        if(debug) Log.d("DEBUG", "public Cursor findDictionaryEntry(String term)");

        Cursor cursor = database.rawQuery("SELECT * FROM "+ DICT_TABLE +" WHERE "+ DICT_TERM +" = ?", new String[]{ term });
        if(cursor!=null){
            cursor.moveToFirst();
        }
        return cursor;
    }

    /**
     * Deletes all rows
     * @return whether or not the operation was successful
     */
    public boolean deleteAllEntries(){
        int rowsDeleted = database.delete(DICT_TABLE, null, new String[]{});

        if(rowsDeleted < 0) return false;
        return true;
    }

    /**
     * Get all entries
     * @return the cursor object with all records
     */
    public Cursor fetchAllEntries(){
        Cursor cursor = database.query(true, DICT_TABLE, new String[] { DICT_TERM, DICT_DEF }, null, null, null, null, null, null);

        if(cursor != null){
            cursor.moveToFirst();
        }

        return cursor;
    }

    /**
     * Drop and create the entire database
     * @return
     */
    public boolean dropAndCreateDb(){
        try {
            helper.onUpgrade(database, 1, 1);
            return true;

        } catch(Exception ex){
            return false;
        }
    }
}