package com.example.faytxzen.simpledictionary;

import android.app.Activity;
import android.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity implements DictionaryFragment.TaskCallbackTarget {
    private static final String TAG_DICTIONARY_FRAGMENT = "dictionary_fragment";
    private EditText mTermTxt, mDefinitionTxt;
    private DictionaryFragment mFragment;

    /** Lifecycle Methods
     *--------------------------------*/

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //retrieve the dictionaryFragment instance if one exists
        FragmentManager fm = getFragmentManager();
        mFragment = (DictionaryFragment) fm.findFragmentByTag(TAG_DICTIONARY_FRAGMENT);

        if(mFragment == null){
            mFragment = new DictionaryFragment();
            fm.beginTransaction().add(mFragment, TAG_DICTIONARY_FRAGMENT).commit();
        }

         //attach the elements to the instance members
        mTermTxt = (EditText)findViewById(R.id.termTxt);
        mDefinitionTxt = (EditText)findViewById(R.id.definitionTxt);
    }


    /** Interface Methods
     *--------------------------------*/
    @Override
    public void onPreExecute() {}

    @Override
    public void onProgressUpdate(String... values) {
        mDefinitionTxt.setText(values[1]);
    }

    @Override
    public void onCancelled() {}

    @Override
    public void onPostExecute() {}


    /* Event Handlers
     * --------------------------------*/
    public void onClick_Lookup(View v){
        if(mFragment != null){
            //trim the inputs
            String term = mTermTxt.getText().toString().trim();
            String def = mDefinitionTxt.getText().toString().trim();

            //if the inputs are valid then start the task
            if(term.length() > 0) mFragment.createTask("find", term.toLowerCase(), def);
            else makeToast("Please provide a valid term!");
        }
    }

    public void onClick_Update(View v){
        if(mFragment != null){
            //trim the inputs
            String term = mTermTxt.getText().toString().trim();
            String def = mDefinitionTxt.getText().toString().trim();

            //if the inputs are valid then start the task
            if(term.length() > 0 && def.length() > 0) mFragment.createTask("update", term, def);
            else makeToast("Please provide a term or definition!");
        }
    }

    public void onClick_rebootDb(View v){
        if(mFragment != null){
            //start the task
            mFragment.createTask("reboot", "", "");
        }
    }

    public void onClick_Clear(View v){
        //reset the EditText fields
        mTermTxt.setText("");
        mDefinitionTxt.setText("");
    }

    /* Action Methods
     *------------------------------*/
    public void makeToast(CharSequence txt){
        Toast.makeText(getApplicationContext(), txt, Toast.LENGTH_SHORT).show();
    }
}
