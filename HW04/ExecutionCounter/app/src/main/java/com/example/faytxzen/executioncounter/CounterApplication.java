package com.example.faytxzen.executioncounter;

import android.app.Activity;
import android.app.Application;
import android.util.Log;

public class CounterApplication extends Application {
    public final static boolean debug = false;
    CounterTask counterTask;
    Activity mTarget;

    @Override
    public void onCreate(){
        super.onCreate();

        if(debug) Log.d("STATUS", "onCreate()");

        counterTask = new CounterTask();
        counterTask.start();
    }

    public void onPauseCounter(){
        if(debug) Log.d("STATUS", "onPauseCounter()");
        if(counterTask != null) counterTask.setPause(true);
    }

    public void onResumeCounter()
    {
        if(debug) Log.d("STATUS", "onResumeCounter()");
        if(counterTask != null) counterTask.setPause(false);
    }

    public void onNotify(final long seconds)
    {
        if(mTarget != null){
            //update the seconds view from MainActivity
            final MainActivity act = ((MainActivity)mTarget);

            act.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    act.updateSeconds(seconds);
                }
            });

        }
    }

    public void setTargetActivity(Activity activity)
    {
        mTarget = activity;
    }

    public class CounterTask extends Thread {
        private boolean pause = false;

        public void setPause(boolean value){
            this.pause = value;
            if(debug) Log.d("STATUS", ""+pause);
        }

        @Override
        public void run(){
            try {
                long seconds = 1;

                while(!interrupted()){
                    //pause condition
                    while(pause){
                        if(debug) Log.d("STATUS", "while(pause)");
                    }

                    Thread.sleep(1000);
                    onNotify(seconds);
                    seconds += 1;

                    if(debug) Log.d("STATUS", "CounterTask.run(), while(!isInterrupted())");
                }

                //give the last seconds count
                onNotify(seconds);
            }
            catch(InterruptedException ie){
                ie.printStackTrace();
            }

            if(debug) Log.d("STATUS", "CounterTask exiting...");
        }
    }

}
