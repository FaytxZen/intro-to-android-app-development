package com.example.faytxzen.executioncounter;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity {
    /*DATA MEMBERS===================*/
    TextView mSecondsTotal;

    /*LIFECYCLE METHODS===================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSecondsTotal = (TextView)findViewById(R.id.secondsCount);

        CounterApplication app = (CounterApplication)getApplication();
        app.setTargetActivity(this);
        app.onResumeCounter();
    }

    @Override
    protected void onPause(){
        super.onPause();
        CounterApplication app = (CounterApplication)getApplication();
        app.onPauseCounter();
    }

    @Override
    protected void onResume(){
        super.onResume();
        CounterApplication app = (CounterApplication)getApplication();
        app.onResumeCounter();
    }

    @Override
    protected void onDestroy(){
        CounterApplication app = (CounterApplication)getApplication();
        app.setTargetActivity(null);
        super.onDestroy();
    }

    @Override
    protected void onStart(){
        super.onStart();
        CounterApplication app = (CounterApplication)getApplication();
        app.onResumeCounter();
    }

    /*INSTANCE METHODS===================*/
    public void updateSeconds(long seconds){
        if(mSecondsTotal != null) mSecondsTotal.setText(Long.toString(seconds));
    }

}
