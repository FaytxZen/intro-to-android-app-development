#CIS4930 Intro to Android Mobile Development

Lecturer: Dave Small

-------------------------------

##February 13, 2015

###Announcements

* None

##Notes

###Memory Leaks: Activities & Threads

####Take 3: No Leaks

```java

	public class MainActivity extends Activity {
		private MyThread mThread

		@Override
		protected void onCreate( Bundle savedInstanceState ){
			super.onCreate(savedInstanceState);
			doSomethingGood();	
		}

		@Override
		protected void onDestroy(){
			super.onDestroy();
			mThread.interrupt();
		}

		private void doSomethingGood(){
			mThread = new MyThread();
			mThread.start();
		}

		private static class MyThread extends Thread {
			private boolean mRunning = false;

			@Override
			public void run(){
				mRunning = true;
				while ( ! interrupted() ){
							try {
								Thread.sleep( 1000 );
							}
							catch( InterruptedException e ){
								break;
							}
				} // end-while
			}
		}
	}

```

This fixes the memory leak issues, but does not solve other issues- like retaining the state after switching screens.


###Handling Activity Configuration Changes with "Headless Fragments"

__Goal__: We want the last created version of an Activity to be the one that receives notifications from a worker Thread/AsyncTask.

The solution has two parts:

####A `TaskFragment`

This `TaskFragment` will be responsible for managing a background task; persists across configuration changes.

```java

	public class TaskFragment extends Fragment {
		
		// things that this can work with must implement the following interface
		static interface TaskCallbackTarget(){
			void onPreExecute();
			void onProgressUpdate(int percent);
			void onCancelled();
			void onPostExecute();
		}

		private TaskCallbackTarget mTarget;
		private DummyTask mTask;

		@Override
		public void onAttach( Activity activity ){
			super.onAttach(activity);

			mTarget = (TaskCallbackTarget ) activity;
		}

		@Override
		public void onCreate( Bundle savedInstanceState ){
			super.onCreate( savedInstanceState );

			// retain this Fragment across configuration changes
			setRetainInstance( true );

			//create the background task and execute it
			mTask = new DummyTask();
			mTask.start();
		}

		@Override
		public void onDetach(){
			super.onDetach();

			mTarget = null;
		}

		private class DummyTask extends AsyncTask<Void, Integer, Void>{
			@Override
			protected void onPreExecute(){
				if ( mTarget != null ){
					mTarget.onPreExecute();
				}
			}

			@Override
			protected Void doInBackground( Void... ignore ){
				for ( int i = 0; !isCancelled() && i != 100; ++i){
					SystemClock.sleep( 100 );
					publishProgress( i );
				}
				return null;
			}

			@Override
			protected void onProgressUpdate( Integer... percent){
				if( mTarget != null ){
					mTarget.onProgressUpdate(percent[0]);
				}
			}

			@Override
			protected void onCancelled() {
				if (mTarget != null){
					mTarget.onCancelled();
				}
			}

			@Override
			protected void onPostExecute( Void ignore ){
				if(mTarget != null){
					mTarget.onPostExecute();
				}
			} 
		}
	}

```

For every `DummyTask` method, the only one executed by `AsyncTask` is `doInBackground(Void... ignore)`. Everything else is done on the UI thread. This means there's no chance for race conditions.