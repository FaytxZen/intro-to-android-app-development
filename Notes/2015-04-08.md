#CIS4930 Intro to Android Mobile Development

Lecturer: Dave Small

-------------------------------

##April 8, 2015

###Announcements

* Still deciding on the final project idea

##Notes

###Extending ContentProvider

Using a class previously defined, `TaskDBHelper`

```java

	public class TaskContentProvider extends ContentProvider {
		private TaskDBHelper db;

		static private final String AUTHORITY = "gov.multiversal.tasksfortigers.provider";
		static private final String BASE_PATH = "tasks";
		static private final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/tasks":
		static private final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/task"; 

		static private final UriMatcher sURIMatcher = new UriMatcher( UriMatcher.NO_MATCH );

		static {
			sUriMatcher.addURI(AUTHORITY, BASE_PATH, TASKS);
			sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", TASK_ID);
		}

		@Override
		public bolean onCreate(){
			db = new TaskDBHelper( getContext() );
			return false;
		}

		@Override
		public Uri insert(Uri uri, ContentValues values){
			int uriType = sURIMatcher.match(uri);
			SQLiteDatabase wdb = db.getWritableDatabase();
			int rowsDeleted = 0;
			long id = 0;

			switch( uriType ){
				case TASKS:
					id = wdb.insert(TaskTable.TASK_TABLE, null, values);
				default:
					throw new IllegalArgumentException("Unknown URI: " + uri);
			}
			getContext().getContentResolver().notifyChange(uri, null);
			return Uri.parse( BASE_PATH + "/" + id );
		}

		@Override
		public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder){
			SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
			checkColumns( projection );

			queryBuilder.setTables( TaskTable.TASK_TABLE );

			int uriType = sURIMatcher.match(uri);

			switch(uriType){
				case TASKS:
					break;
				case TASK_ID:
					queryBuilder.appendWhere(TaskTable.COLUMN_ID + "=" + uri.getLastPathSegment() );
			}

			SQLiteDatabase wdb = db.getWritableDatabase();
			Cursor cursor = queryBuilder.query<( 
				wdb, projection, selection, selectionArgs, 
				null, null, sortOrder);

			cursor.setNotificationUri( getContext().getContentResolver(), uri );

			return cursor;
		}

		public int update( Uri uri, ContentValues values, String selection, String[] selectedArgs ){
			int uriType = sURIMatcher.match(uri);
			SQLiteDatabase wdb = db.getWritableDatabase();
			int rowsUpdated = 0;

			switch( uriType ){
				case TASKS:
					break;
				case TASKS_ID:
					break;
				default:
					throw new IllegalArgumentException("Unknown URI: " + uri);
			}
		}

		private void checkColumns( String[] projections ){
			String[] available = { 
				TaskTable.COLUMN_ID, 
				TaskTable.COLUMN_PRIORITY, 
				TaskTable.COLUMN_SUMMARY,
				TaskTable.COLUMN_DESCRIPTION
			};
			
			if(projections != null ){
				HashSet<String> requestedColumns = new HashSet<>( projections );
				HashSet<String> availableColumns = new HashSet<>( available );
				
				if( !availableColumns.containsAll(requestedColumns)){
					throw new IllegalArgumentException("Unknown columns in projection");
				}
			}
		}

		//Other methods and overrides
	}

```