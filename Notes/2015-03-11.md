#CIS4930 Intro to Android Mobile Development

Lecturer: Dave Small

-------------------------------

##March 11, 2015

###Announcements

* If you got a 0 for project 1, contact Dave or the TAs.

##Notes

###SQLite Overview (cont.)

####Using a `Cursor`

```java

	cursor.moveToFirst();

	while( !cursor.isAfterlast() ){
		X var = cursor.getX( column_index ); // indexes start at 0

		cursor.moveToNext();
	}

	cursor.close(); //always close it

```

__Types of data that can be retrieved:__

* `getBlob()` => `byte[]`
* `getDouble()` => `double`
* `getFloat()` => `float` cast from SQLite's "double"
* `getInt()` => `int` cast from SQLite's "long"
* `getShort()`
* `getLong()`
* `getString()`

