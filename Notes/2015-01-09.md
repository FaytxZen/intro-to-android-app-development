#CIS4930 Intro to Android Mobile Development

Lecturer: Dave Small

-------------------------------

##January 9, 2015

###Announcements

* Download Android Studio, if you haven't already.
* No exams, but there will be quizzes
* Currently, only one project is really planned
* Homework will be graded by lottery
* 40% quizzes, 60% homeworks + project

##Notes

Dave showed us some features of Android Studio.