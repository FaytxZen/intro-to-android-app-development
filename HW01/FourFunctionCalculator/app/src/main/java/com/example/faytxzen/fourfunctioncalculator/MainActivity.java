package com.example.faytxzen.fourfunctioncalculator;

import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {
    //CONSTANTS==================================
    private static final String CURRENT_ENTRY = "CURRENT_ENTRY";
    private static final String EQUATION_ENTRY = "EQUATION_ENTRY";
    private static final String LAST_ACTION_STRING = "LAST_ACTION_STRING";
    private static final String CAN_CLEAR_FLAG = "CAN_CLEAR_FLAG";
    private static final String idempotentCheckString = "+-*/";

    //ELEMENTS===================================
    private EditText mNumDisplay;
    private TextView mEqnDisplay;

    //DATA MEMBERS===================================
    private String lastAction;
    private boolean canClear;

    //OVERRODE METHODS===================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String currentEntry = getPreferences(MODE_PRIVATE).getString(CURRENT_ENTRY, "0");
        String equationEntry = getPreferences(MODE_PRIVATE).getString(EQUATION_ENTRY, "");
        lastAction = getPreferences(MODE_PRIVATE).getString(LAST_ACTION_STRING, "");
        canClear = getPreferences(MODE_PRIVATE).getBoolean(CAN_CLEAR_FLAG, true);

        mNumDisplay = (EditText)findViewById(R.id.num_display);
        mNumDisplay.setText(currentEntry);
        mEqnDisplay = (TextView)findViewById(R.id.eqn_display);
        mEqnDisplay.setText(equationEntry);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences.Editor edit = getPreferences(MODE_PRIVATE).edit();
        edit.putString(CURRENT_ENTRY, mNumDisplay.getText().toString());
        edit.putString(EQUATION_ENTRY, mEqnDisplay.getText().toString());
        edit.putBoolean(CAN_CLEAR_FLAG, canClear);
        edit.putString(LAST_ACTION_STRING, lastAction);
        edit.apply();
    }

    //UTILITY METHODS===================================

    /**
     * Appends text to the display value. If the display value is 0, it sets it to toAppend
     * @param toAppend the text to append to the display
     */
    private void appendText(String toAppend){
        if(toAppend == null) return;

        //if nNumDisplay is 0 or canClear is true- replace it with toAppend
        if((mNumDisplay.getText().toString().equals("0") || canClear) && !idempotentCheckString.contains(toAppend)){
            mNumDisplay.setText(toAppend);
            if(canClear) canClear = false;
            return;
        }

        //if toAppend is a digit, append it at the end
        if(!idempotentCheckString.contains(toAppend)){
            mNumDisplay.setText(mNumDisplay.getText()+toAppend);
        }
        else { //if toAppend is an operator
            //if mEqnDisplay is set up, perform calculation with the current nNumDisplay
            if(mEqnDisplay.getText().toString().isEmpty()){
                mEqnDisplay.setText(mNumDisplay.getText()+" "+toAppend+" ");
                canClear = true;
            }
        }
    }

    /**
     * Toggles the polarity of the display value, i.e. multiplies it by -1
     */
    private void togglePolarity(){
        String currentValueString = mNumDisplay.getText().toString();
        if(currentValueString.isEmpty()) return;

        float currentValue = Float.valueOf(currentValueString);
        mNumDisplay.setText(formatValue(currentValue * -1));
    }

    /**
     * Formats the float to the lowest precision while maintaining the same value
     * If the value is like 12.0, it will format it into an integer
     * @param value the value to be formatted
     * @return returns the value formatted as a String
     */
    private String formatValue(float value){
        if(value == (int)value) return String.format("%d", (int)value);
        else return String.format("%s", value);
    }

    /**
     * Clears the displayed value, i.e. sets it to 0
     */
    private void clearEntry(){
        mNumDisplay.setText("0");
    }

    /**
     * Clears all variables
     */
    private void clearAll(){
        lastAction = "";
        mEqnDisplay.setText("");
        clearEntry();
    }

    /**
     * Backspaces the displayedValue by 1 character
     * If the value is a single digit, it sets it to 0
     */
    private void backspaceText(){
        if(canClear) return;
        if(mNumDisplay.getText().length() < 2) {
            mNumDisplay.setText("0");
        }
        else {
            mNumDisplay.setText(mNumDisplay.getText().subSequence(0, mNumDisplay.getText().length() - 1));
        }
    }

    /**
     * Parses the equation string and returns the evaluated expression
     * @return the evaluated value
     */
    private float parseEqnString(String eqn){
        String[] symbols = eqn.trim().split(" ");

        float prev = Float.valueOf(symbols[0]), cur = Float.valueOf(symbols[2]);

            switch(symbols[1]){
                case "+":
                    prev += cur;
                    break;
                case "-":
                    prev -= cur;
                    break;
                case "/":
                    prev /= cur;
                    break;
                case "*":
                    prev *= cur;
                    break;
            }
        lastAction = symbols[1] + " " + symbols[2] + " ";
        return prev;
    }

    /**
     * Handle the cases for calculation
     */
    private void calculate(){
        try{
            float result;

            //if mEqnDisplay is empty
            if(mEqnDisplay.getText().toString().isEmpty()){
                if(!lastAction.isEmpty() && canClear){ //handle the repeat operation case
                    result = parseEqnString(mNumDisplay.getText().toString() + " " + lastAction);
                }
                else return;
            }
            else{ //just parse and compute as normal
                result = parseEqnString(mEqnDisplay.getText().toString() + mNumDisplay.getText().toString());
            }

            //set the result as the currently displayed text
            mNumDisplay.setText(formatValue(result));
            mEqnDisplay.setText("");
            canClear = true; //you can clear the entry after pressing '='

        } catch(ArithmeticException ae){
            mNumDisplay.setText("ArithmeticException occurred.");
        }

    }

    //EVENT LISTENERS===================================

    /**
     *Handles all events sent from elements in activity_main.xml
     * @param v the View object that sent the event
     */
    public void buttonClickHandler(View v){
        switch(v.getId()){
            case R.id.btn_backspace:
                backspaceText();
                break;
            case R.id.btn_clear_all:
                clearAll();
                break;
            case R.id.btn_clear_entry:
                clearEntry();
                break;
            case R.id.btn_decimal:
                if(!mNumDisplay.getText().toString().contains(".")) appendText(".");
                break;
            case R.id.btn_one:
                appendText("1");
                break;
            case R.id.btn_two:
                appendText("2");
                break;
            case R.id.btn_three:
                appendText("3");
                break;
            case R.id.btn_four:
                appendText("4");
                break;
            case R.id.btn_five:
                appendText("5");
                break;
            case R.id.btn_six:
                appendText("6");
                break;
            case R.id.btn_seven:
                appendText("7");
                break;
            case R.id.btn_eight:
                appendText("8");
                break;
            case R.id.btn_nine:
                appendText("9");
                break;
            case R.id.btn_zero:
                appendText("0");
                break;
            case R.id.btn_add:
                appendText("+");
                break;
            case R.id.btn_subtract:
                appendText("-");
                break;
            case R.id.btn_divide:
                appendText("/");
                break;
            case R.id.btn_multiply:
                appendText("*");
                break;
            case R.id.btn_equals:
                calculate();
                break;
            case R.id.btn_plus_minus:
                togglePolarity();
                break;
            default:
                break;
        }
    }
}
