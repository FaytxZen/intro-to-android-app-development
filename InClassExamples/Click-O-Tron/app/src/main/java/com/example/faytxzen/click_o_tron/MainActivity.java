package com.example.faytxzen.click_o_tron;

import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    private final String CURRENT_COUNT_KEY = "storedCount";
    private long mCurrentCount = 0;
    TextView mTitle, mCount;
    Button mIncBtn, mResetBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mCurrentCount = getPreferences(MODE_PRIVATE).getLong(CURRENT_COUNT_KEY,0);

        mTitle = (TextView)findViewById(R.id.titleText);
        mCount= (TextView)findViewById(R.id.clickReadout);
        updateCounterText();

        mIncBtn = (Button)findViewById(R.id.inc_btn);
        mIncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickButtonHandler(v);
            }
        });
        mResetBtn = (Button)findViewById(R.id.reset_btn);
        mResetBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                clearButtonHandler(v);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause(){
        super.onPause();

        SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
        editor.putLong(CURRENT_COUNT_KEY, mCurrentCount);
        editor.commit();
    }


    public void clickButtonHandler(View view){
        ++mCurrentCount;
        updateCounterText();
    }
    public void clearButtonHandler(View view){
        mCurrentCount = 0;
        updateCounterText();
    }
    private void updateCounterText(){
        //reference to the textview
        mCount.setText( String.valueOf(mCurrentCount));
    }
}
