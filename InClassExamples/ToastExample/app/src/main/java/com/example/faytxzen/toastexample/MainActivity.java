package com.example.faytxzen.toastexample;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {
    private Button mSimpleToastBtn, mFancyToastBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mFancyToastBtn = (Button)findViewById(R.id.toast_btn_alt);
        mFancyToastBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                fancy_button_handler(v);
            }
        });
        mSimpleToastBtn = (Button)findViewById(R.id.toast_btn);
        mSimpleToastBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                simple_button_handler(v);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void simple_button_handler(View view){
        CharSequence message = getResources().getText(R.string.simple_toast_message);
        simpleToast(message);
    }

    public void fancy_button_handler(View view){
        CharSequence message = getResources().getString(R.string.fancy_toast_message);
        fancyToast(message);
    }

    /***
     * Create a simple toast with the given CharSequence
     * @param text The message to display in the toast
     */
    private void simpleToast(CharSequence text){
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }


    private void fancyToast(CharSequence text){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)findViewById(R.id.toast_layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.image);
        image.setImageResource(R.drawable.ic_launcher);

        TextView textView = (TextView)layout.findViewById(R.id.text);
        textView.setText(text);

        int duration = Toast.LENGTH_LONG;

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();
    }
}
