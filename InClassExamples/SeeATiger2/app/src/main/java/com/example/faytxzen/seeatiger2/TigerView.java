package com.example.faytxzen.seeatiger2;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by faytxzen on 1/12/15.
 */
public class TigerView extends View {
    Bitmap mTigerBitmap = null;
    int mOrientation;

    public TigerView(Context context, Bitmap pic, int orientation){
        super(context);
        mTigerBitmap = pic;
        mOrientation = orientation;
    }

    @Override
    protected void onDraw(Canvas canvas){
        //called when the View is drawn
        Paint paint = new Paint();
        paint.setFilterBitmap(true);

        //scale the picture
        Rect destShape;
        int viewWidth = this.getWidth();
        int viewHeight = this.getHeight();
        int bmWidth = mTigerBitmap.getWidth();
        int bmHeight = mTigerBitmap.getHeight();

        if(mOrientation == Configuration.ORIENTATION_LANDSCAPE){
            destShape = new Rect(0,0, viewWidth, bmHeight*viewWidth/bmWidth);
        }
        else {
            destShape = new Rect(0,0, bmWidth*viewHeight/bmHeight, viewHeight);
        }


        if(mTigerBitmap != null) canvas.drawBitmap(mTigerBitmap, null, destShape, paint);
    }
}
