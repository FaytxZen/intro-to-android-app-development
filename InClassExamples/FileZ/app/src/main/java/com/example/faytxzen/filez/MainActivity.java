package com.example.faytxzen.filez;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;


public class MainActivity extends ActionBarActivity {
    private final String internalFileName = "file_data.txt";
    private Button mJokeBtn;
    private TextView mHeadlineView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mJokeBtn = (Button)findViewById(R.id.joke_btn);
        mHeadlineView = (TextView)findViewById(R.id.headline);

        mJokeBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                mHeadlineView.setText(readFileFromInternalStorage());
            }
        });

        writeFileToInternalStorage();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //write a file to internal storage
    private void writeFileToInternalStorage(){
        PrintWriter writer = null;

        try {
            FileOutputStream fos = openFileOutput(internalFileName, MODE_WORLD_READABLE);

            //Wrap the outputstream in a bufferedoutputstream to read it consistently from the system
            //buffer rather than whatever bytes the system is currently reading
            //The system can be feeding single chars or chunks, who knows
            writer = new PrintWriter(new BufferedOutputStream(fos));

            writer.println("What's better than a Tygra?");
            writer.println();
            writer.println("Two Tygras.");

        } catch(Exception e){
            e.printStackTrace();
        } finally {
            if(writer != null) writer.close(); //only need to call this one, because .close() propogates
        }
    }

    //read a file to internal storage
    private CharSequence readFileFromInternalStorage(){
        String eol = System.getProperty("line.separator");
        StringBuilder builder = new StringBuilder();

        BufferedReader input = null;

        try {
            FileInputStream fis = openFileInput(internalFileName);
            input = new BufferedReader(new InputStreamReader(fis));

            String line;
            while( (line = input.readLine()) != null ) {
                builder.append(line);
                builder.append(eol);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(input!=null) {
                try {
                    input.close();
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }

            return builder.toString();
        }
    }
}
