package com.example.faytxzen.tigerverifier;

import android.app.Activity;
import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class UrlVerifierApplication extends Application {
    public static final boolean debug = false;
    private Activity mActivity;

    public void setActivity(Activity activity){
        this.mActivity = activity;
    }

    /**
     * Runs the UrlVerifierTask to check if the given url satisfies the condition (has tigers)
     * @param url The url to be verified
     */
    public void verifyUrl(String url){
        if(debug) Log.d("CHECKPOINT", "void verifyUrl(String url)");

        //create an instance of the AsyncTask and execute it
        UrlVerifierTask uvt = new UrlVerifierTask(url);
        uvt.execute();
    }

    /**
     * Checks the given URL and appends "http://" if necessary
     * @param url The URL to be verified
     * @return The verified/corrected URL
     */
    public static String httpUrlify(String url){
        String result = url;

        if(!url.toLowerCase().contains("http://")){
            result = "http://"+result;
        }

        return result;
    }

    /**
     * Makes a simple toast with the given message
     * @param msg The message to show in the toast
     */
    public void showToast(final String msg){
        if(mActivity != null) {
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(debug) Log.d("TEST", "run()");
                    Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    /**
     * Private inner class for verifying Urls in the background
     */
    private class UrlVerifierTask extends AsyncTask<Void, Void, Void> {
        private String url = null;

        public UrlVerifierTask(String url){
            this.url = url;
        }

        /**
         * Determines if the url contains any mention of tigers
         * @param params Ignored params
         * @return null
         */
        @Override
        protected Void doInBackground(Void... params) {
            if(debug) Log.d("CHECKPOINT", "Void doInBackground(Void... params)");

            try {
                //create instances of the objects we'll need
                URL url = new URL(httpUrlify(this.url));
                Pattern pattern = Pattern.compile("(tiger|tigers)", Pattern.CASE_INSENSITIVE);
                BufferedReader in = new BufferedReader( new InputStreamReader( url.openStream() ) );

                Matcher matcher;
                String line;

                //while there is something to read, process the input
                while((line=in.readLine())!=null){
                    //create Matcher instance for the regex defined above
                    matcher = pattern.matcher(line);

                    //if any match is found and the Activity member isn't null
                    if(matcher.find() && mActivity != null) {
                        //make an affirmative toast
                        mActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ((MainActivity)mActivity).makePositiveToast();
                            }
                        });

                        return null;
                    }
                }

                //otherwise make a negative toast
                if(mActivity != null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity)mActivity).makeNegativeToast();
                        }
                    });
                }
            }
            catch(MalformedURLException me){
                me.printStackTrace();
                showToast("Not a valid URL. Try again.");
                if(mActivity != null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity)mActivity).setBtnDefaultState();
                        }
                    });
                }
            }
            catch(IOException ioe){
                ioe.printStackTrace();
                showToast("Issue reading from the URL. Verify it\'s valid.");
                if(mActivity != null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ((MainActivity)mActivity).setBtnDefaultState();
                        }
                    });
                }
            }

            return null;
        }
    }
}
