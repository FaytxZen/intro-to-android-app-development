package com.example.faytxzen.tigerverifier;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;


public class MainActivity extends Activity {
    /* Data Members
     *---------------------------------*/
    private final String ENABLE_OPEN = "ENABLE_OPEN_LINK_BTN_VALUE";
    private final String LAST_URL = "LAST_URL_ENTERED";
    private final String positive_msg = "This site has tigers! It\'s recommended to proceed!";
    private final String negative_msg = "This site has no tigers. It\'s probably not that good.";
    private EditText mUserInput;
    private Button mOpenLinkBtn, mVerifyBtn;

    /* Lifecycle Methods
     *---------------------------------*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(UrlVerifierApplication.debug) Log.d("CHECK", "onCreate(Bundle savedInstanceState)");

        //get the View objects and set any event listeners
        mUserInput = (EditText)findViewById(R.id.user_input);
        mUserInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                onTextChanged_update();
            }
        });
        mOpenLinkBtn = (Button)findViewById(R.id.open_link_btn);
        mVerifyBtn = (Button)findViewById(R.id.verify_btn);

        //restore any SharedPreferences
        mUserInput.setText(getPreferences(MODE_PRIVATE).getString(LAST_URL, ""));
        mOpenLinkBtn.setEnabled(getPreferences(MODE_PRIVATE).getBoolean(ENABLE_OPEN, false));

        //set the Application instance's mActivity to this instance
        UrlVerifierApplication uva = (UrlVerifierApplication)getApplicationContext();
        uva.setActivity(this);
    }

    @Override
    protected void onPause(){
        super.onPause();
        if(UrlVerifierApplication.debug) Log.d("CHECK", "onPause()");
        //save the user-inputted url and open_link IsEnabled status to SharedPreferences
        SharedPreferences.Editor edit = getPreferences(MODE_PRIVATE).edit();
        edit.putString(LAST_URL, mUserInput.getText().toString());
        edit.putBoolean(ENABLE_OPEN, mOpenLinkBtn.isEnabled());
        edit.apply();
    }

    @Override
    protected void onResume(){
        super.onResume();
        if(UrlVerifierApplication.debug) Log.d("CHECK", "onResume()");
        mOpenLinkBtn.setEnabled(getPreferences(MODE_PRIVATE).getBoolean(ENABLE_OPEN, false));
    }

    @Override
    protected void onDestroy(){
        //dereference the current MainActivity instance from the application instance
        UrlVerifierApplication uva = (UrlVerifierApplication)getApplicationContext();
        uva.setActivity(null);

        super.onDestroy();
    }

    /* EventHandlers
     *---------------------------------*/

    /**
     * ClickHandler for the Verify button
     * @param v The view of the bound view object
     */
    public void onClick_verify(View v){
        UrlVerifierApplication uva = (UrlVerifierApplication)getApplicationContext();
        uva.verifyUrl(mUserInput.getText().toString());

        v.setEnabled(false);
        mUserInput.setEnabled(false);
    }

    /**
     * ClickHandler for the OpenLink button
     * @param v The view of the bound object
     */
    public void onClick_open(View v){
        //open the URL
        openUrl(mUserInput.getText().toString());

        //disable the OpenLink button
        mOpenLinkBtn.setEnabled(false);
    }

    /**
     * TextListener for the EditText view
     */
    public void onTextChanged_update(){
        if(mOpenLinkBtn != null) mOpenLinkBtn.setEnabled(false);
    }

    /* Utility Methods
     *---------------------------------*/

    /**
     *
     */
    public void setBtnDefaultState(){
        mUserInput.setEnabled(true);
        mVerifyBtn.setEnabled(true);
        mOpenLinkBtn.setEnabled(false);
    }


    /**
     * Sends an intent to start a Browser activity
     * @param url the URL to open
     */
    public void openUrl(String url){
        if(UrlVerifierApplication.debug) Log.d("CHECKPOINT", "void openUrl(String url)");

        //Make sure the url has an "http://" prefix
        Uri site = Uri.parse(UrlVerifierApplication.httpUrlify(url));

        Intent intent = new Intent( Intent.ACTION_VIEW, site);

        PackageManager pm = getPackageManager();

        List<ResolveInfo> activities = pm.queryIntentActivities(intent, 0);

        //if any activities are appropriate, call startActivity
        if(activities.size() > 0){
            startActivity(intent);
        }
    }

    /**
     * Make an affirmative fancy toast
     */
    public void makePositiveToast(){
        makeFancyToast(positive_msg, R.drawable.happy_calvin_hobbes);
    }

    /**
     * Make a negative fancy toast
     */
    public void makeNegativeToast(){
        makeFancyToast(negative_msg, R.drawable.cautionary_hobbes);
    }

    /**
     * Function to make a fancy toast
     * @param text The text to display in the Toast view
     * @param id The id of the image to show in the Toast view
     */
    private void makeFancyToast(CharSequence text, int id){
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.toast_layout, (ViewGroup)findViewById(R.id.toast_layout_root));
        ImageView image = (ImageView) layout.findViewById(R.id.toast_img);
        image.setImageResource(id);

        TextView textView = (TextView)layout.findViewById(R.id.toast_txt);
        textView.setText(text);

        int duration = Toast.LENGTH_SHORT;

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setDuration(duration);
        toast.setView(layout);
        toast.show();

        //enable the views after response
        mOpenLinkBtn.setEnabled(true);
        mUserInput.setEnabled(true);
        mVerifyBtn.setEnabled(true);
    }
}
