package com.example.faytxzen.piformontecarlo;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity implements TaskFragment.TaskCallbackTarget {
    /*DATA MEMBERS==========================================*/
    public static final boolean debug = true;
    private static EditText mCurrentValueText, mTargetCycleText, mHitRatioText;
    private TaskFragment mFragment;
    private static final String TAG_TASK_FRAGMENT = MainActivity.class.getName();
    private final String TOTAL_CYCLES = "TOTAL_RANDOM_POINTS";


    /*LIFECYCLE METHODS=====================================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //get the fragment manager and task fragment
        FragmentManager fm = getFragmentManager();
        mFragment = (TaskFragment)fm.findFragmentByTag(TAG_TASK_FRAGMENT);

        //create and add the task fragment if it doesn't exist
        if(mFragment == null){
            mFragment = new TaskFragment();
            fm.beginTransaction().add(mFragment, TAG_TASK_FRAGMENT).commit();
        }

        //get the view instances
        mCurrentValueText = (EditText)findViewById(R.id.currentValue);
        mTargetCycleText = (EditText)findViewById(R.id.targetNumCycles);
        mHitRatioText = (EditText)findViewById(R.id.hitRatio);

        //load up SharedPreferences values
        int totalPoints = getPreferences(MODE_PRIVATE).getInt(TOTAL_CYCLES, 0);

        //set the values
        mTargetCycleText.setText(formatValue(totalPoints));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveSharedPreferences(); //save any necessary values
    }

    /*METHODS=====================================*/

    /**
     * Saves the needed values to SharedPreferences
     */
    private void saveSharedPreferences(){
        SharedPreferences.Editor edit = getPreferences(MODE_PRIVATE).edit();
        edit.putInt(TOTAL_CYCLES, Integer.valueOf(mTargetCycleText.getText().toString()));

        edit.apply();
    }

    /**
     * The onClick handler for the Start button
     * @param v A View object
     */
    public void onClickStart(View v){
        //check if the user has really entered a number
        try {
            Integer.parseInt(mTargetCycleText.getText().toString());
            mFragment.start(); //execute the fragment AsyncTask
        }
        catch(NumberFormatException e){
            //make them know their error
            makeErrorToast();
        }
    }

    /**
     * The onClick handler for the plus button
     * @param v A View object
     */
    public void onClickIncTargetCycles(View v){
        updateTargetCycle(true);
    }

    /**
     * The onClick handler for the minus button
     * @param v A View object
     */
    public void onClickDecTargetCycles(View v){
        updateTargetCycle(false);
    }

    /**
     * The shared Method for adding or subtracting the current TargetCycle value
     * @param add Booleans specifying whether to add or subtract, true is add
     */
    private void updateTargetCycle(boolean add){
        int targetCycle;

        try {
            targetCycle = Integer.parseInt(mTargetCycleText.getText().toString());
        }
        catch(NumberFormatException e){
            makeErrorToast();
            return;
        }
        int step = TaskFragment.step;
        int newTargetCycle = add ? targetCycle + step : targetCycle - step;

        if(newTargetCycle >= 0){
            mTargetCycleText.setText(formatValue(newTargetCycle));
        }
    }

    /**
     * Clears everything
     * @param v A View object
     */
    public void onClickClearAll(View v){
        if(debug) Log.d("RUNNING", "onClickClearAll(View v)");
        TaskFragment.setRunning(false);
        mHitRatioText.setText("0");
        mCurrentValueText.setText("0");
        mTargetCycleText.setText("0");
    }

    /**
     * Formats decimal values into pretty strings
     * @param value the floating point value to be converted
     * @return the pretty string
     */
    private String formatValue(double value){
        if(value == (int)value) return String.format("%d", (int)value);
        return String.format("%.10f", value);
    }

    /**
     * Tell the user for shame
     */
    private void makeErrorToast(){
        //make them know their error
        CharSequence msg = "Your input was bad. I'm sorry.";
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * @return get the current target cycle
     */
    public static int getTargetCycle(){
        return Integer.valueOf(mTargetCycleText.getText().toString());
    }

    /**
     * @return get the current hit ratio
     */
    public static double getHitRatio(){
        return Double.valueOf(mHitRatioText.getText().toString());
    }


    /*INTERFACE METHODS===================================*/

    @Override
    public void onPreExecute() {
        if(MainActivity.debug) Log.d("RUNNING", "onPreExecute()");
    }

    @Override
    public void onProgressUpdate(double value) {
        if(MainActivity.debug) Log.d("RUNNING", "onProgressUpdate(double value)");

        //update hit ratio and current estimation
        mHitRatioText.setText(formatValue(value));
        mCurrentValueText.setText(formatValue(value * 4));
    }

    @Override
    public void onCancelled() {
        TaskFragment.setRunning(false);
    }

    @Override
    public void onPostExecute(double value) {
        if(MainActivity.debug) Log.d("RUNNING", "onPostExecute(double value)");

        //final update
        if(value != 0) onProgressUpdate(value);
    }
}
