package com.example.faytxzen.piformontecarlo;

import android.app.Activity;
import android.app.Fragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.util.Pair;
import android.util.Log;

import java.util.Random;


public class TaskFragment extends Fragment {

    //inner interface to enforce MonteCarloTask calls
    static interface TaskCallbackTarget {
        void onPreExecute();
        void onProgressUpdate(double value);
        void onCancelled();
        void onPostExecute(double value);
    }

    /*DATA MEMBERS===========================*/
    public static final int step = 1000;
    private TaskCallbackTarget mTarget;
    private MonteCarloTask mTask;
    private static boolean mRunning = false;

    /*UTILITY METHODS===========================*/
    public static void setRunning(boolean running){
        mRunning = running;
    }

    public static boolean isRunning(){
        return mRunning;
    }

    /*INSTANCE METHODS===========================*/
    public void start(){
        if(!mRunning){
            mTask = new MonteCarloTask();
            mTask.execute();
            mRunning = true;
        }
    }

    /*OVERRIDEN METHODS===========================*/
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        mTarget = (TaskCallbackTarget)activity;
    }

    @Override
    public void onCreate( Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        //retain this fragment across configuration changes
        setRetainInstance(true);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mTarget = null;
    }

    @Override
    public void onDestroy(){
        mRunning = false;
        super.onDestroy();
    }


    //Private inner class for defining the AsyncTask for the Monte Carlo simulation
    private class MonteCarloTask extends AsyncTask<Void, Double, Void> {
        int currentCycle;
        double hitRatio;

        /**
         * Check if the given Pair is in a circle of radius 1
         * @param coord The Pair object
         * @return Whether coord was in the circle
         */
        private boolean isInCircle(Pair<Double, Double> coord){
            //checks for quadrant 1

            //get the squared values for x and y
            double x2 = Math.pow(coord.first, 2);
            double y2 = Math.pow(coord.second, 2);

            //check if x2 + y2 <= 1, a property of circles

            return x2 + y2 <= 1;
        }

        /**
         * Generates a Pair<X, Y> where 0 <= X <= 1, 0 <= Y <= 1
         * @return A Pair Object with XY coordinates as doubles
         */
        private Pair<Double, Double> generateRandomCoords(){
            double x, y;

            //generate numbers between 0 and 1, inclusive
            Random rand = new Random();
            x = rand.nextDouble();
            y = rand.nextDouble();

            //create and return a pair object
            return new Pair<>(x,y);
        }


        @Override
        protected void onPreExecute(){
            if(mTarget != null){
                mTarget.onPreExecute();
            }
        }

        @Override
        protected Void doInBackground(Void... ignore){
            if(MainActivity.debug) Log.d("RUNNING", "doInBackground(Void... ignore)");

            mRunning = true;
            currentCycle = 0;

            double hitRatio = MainActivity.getHitRatio();
            int targetCycle = MainActivity.getTargetCycle();

            while(++currentCycle <= targetCycle && mRunning){

                //set to whatever it is currently, in case the user ups it
                targetCycle = MainActivity.getTargetCycle();

                //plot a point and make the check
                if(isInCircle(generateRandomCoords())) {
                    hitRatio = ((hitRatio * (currentCycle - 1)) + 1)/currentCycle;
                }
                else {
                    hitRatio = (hitRatio * (currentCycle - 1))/currentCycle;
                }

                //make the request to update the UI every `step` steps
                if(currentCycle % step == 0) {
                    publishProgress(hitRatio);
                } //end-if
                //thread exits
            } //end-while

            mRunning = false;

            return null;
        }

        @Override
        protected void onProgressUpdate(Double... value){
            if(mTarget != null){
                mTarget.onProgressUpdate(value[0]);
            }
        }

        @Override
        protected void onCancelled(){
            if(mTarget != null){
                mTarget.onCancelled();
            }
        }

        @Override
        protected void onPostExecute( Void ignore ){
            if(mTarget != null){
                mTarget.onPostExecute(hitRatio);
            }
        }
    }
}
