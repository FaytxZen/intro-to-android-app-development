package com.example.faytxzen.colordoctor.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.example.faytxzen.colordoctor.MainActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by faytxzen on 4/20/15.
 */
public class ColorHelper extends SQLiteOpenHelper {
    /* DATA MEMBERS
     *=============================*/
    public static final boolean DEBUG = MainActivity.DEBUG;
    public static final String
        DATABASE_NAME = "ColorDoctor.db",
        TABLE_NAME = "HSVColors",
        ID = "_id",
        COLOR_NAME_COLUMN = "colorNm",
        HUE_COLUMN = "hue",
        SATURATION_COLUMN = "saturation",
        VALUE_COLUMN = "value";
    public static final int DATABASE_VERSION = 1;
    private static final String CREATE_COLOR_TABLE = String.format("CREATE TABLE IF NOT EXISTS %s (%s INTEGER, %s TEXT, %s REAL, %s REAL, %s REAL)",
            TABLE_NAME, ID, COLOR_NAME_COLUMN, HUE_COLUMN, SATURATION_COLUMN, VALUE_COLUMN);
    private static final String UPGRADE_COLOR_TABLE = String.format("DROP TABLE IF EXSITS %s", TABLE_NAME);

    /* CONSTRUCTORS
     *=============================*/
    public ColorHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onCreate(SQLiteDatabase db) {
        if(DEBUG) Log.d("ColorHelper", "public void onCreate( SQLiteDatabase db)");
        db.execSQL(CREATE_COLOR_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(UPGRADE_COLOR_TABLE);
        onCreate(db);
    }

    /* SEEDER METHODS
     *=============================*/
    public void seedDatabaseWithColors(InputStreamReader isr){
        SQLiteDatabase db = this.getWritableDatabase();

        BufferedReader reader = null;

        try {
            reader = new BufferedReader(isr);
            String line;

            while( (line = reader.readLine()) != null ){
                String[] terms = line.split(",");

                ContentValues values = new ContentValues();
                values.put(COLOR_NAME_COLUMN, terms[0]);
                values.put(HUE_COLUMN, Float.parseFloat(terms[1]));
                values.put(SATURATION_COLUMN, Float.parseFloat(terms[2])/100.0);
                values.put(VALUE_COLUMN, Float.parseFloat(terms[3])/100.0);

                long id = db.insert(TABLE_NAME, null, values);
                if(DEBUG) Log.d("ColorHelper", String.format("Inserting record with id %d", id));
            }
        }
        catch(IOException ioe){
            if(DEBUG) ioe.printStackTrace();
        }
        catch(NumberFormatException nfe){
            if(DEBUG) Log.d("ColorHelper", "NumberFormatException: " + nfe.toString());
        }
        finally {
            if(reader != null){
                try{
                    reader.close();
                } catch(IOException ioe){}
            }
        }
    }
}
