package com.example.faytxzen.colordoctor.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;
import com.example.faytxzen.colordoctor.adapters.HSVAdapter;
import com.example.faytxzen.colordoctor.comparators.HSVComparators;
import com.example.faytxzen.colordoctor.contentproviders.ColorProvider;
import com.example.faytxzen.colordoctor.database.ColorHelper;
import com.example.faytxzen.colordoctor.drawables.HSVGradientDrawable;
import com.example.faytxzen.colordoctor.generators.HSVGradients;
import com.example.faytxzen.colordoctor.models.ColorRecord;

import java.util.ArrayList;

public class NamedColorFragment extends Fragment {
    /* DATA MEMBERS
     *=============================*/
    private final boolean DEBUG = MainActivity.DEBUG;
    private final String LIST_TAG = "LIST_TAG";
    private final String SORT_ORDER = "SORT_ORDER";
    private final String ALERT_DIALOG = "ALERT_DIALOG";
    private String[] params;
    private ColorRecord[] colorRecords;
    private ListView mListView;
    private Button mSortOrderBtn;
    private int preferredSortOrder;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray(LIST_TAG, params);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_named_listview, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //check if there are any existing instances or this fragment, else use any supplied parameters
        if(savedInstanceState != null){
            params = savedInstanceState.getStringArray(LIST_TAG);
        }
        else {
            MainActivity.FragmentMessage msg = ((MainActivity) getActivity()).getFragmentMessage();
            if(msg != null){
                params = msg.getMsg();
            }
        }

        //get the colors from the database matching the passed in conditions
        float[] hues = getHues(params[0]);
        float saturation = Float.parseFloat(params[1]);
        float value = Float.parseFloat(params[2]);
        float delta = HSVGradients.getDelta(MainActivity.NUM_ITEMS);

        HSVGradientDrawable[] gradients = getGradients(getColorRecords(getColors(hues[0], hues[1], saturation, value, delta)));
        if(gradients.length == 0)
            Toast.makeText(getActivity().getApplicationContext(), "No results were found!", Toast.LENGTH_SHORT).show();

        //load the list view with the gradients and attach onItemClickListeners
        mListView = (ListView) getView().findViewById(R.id.swatch_list);
        mListView.setAdapter(new HSVAdapter(getActivity(), gradients));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ColorRecord record = colorRecords[position];
                String info = String.format("Name: %s, Hue: %s, Saturation: %s, Value: %s", record.name, record.hue, record.saturation, record.value);
                Toast.makeText(getActivity().getApplicationContext(), info, Toast.LENGTH_SHORT).show();
            }
        });

        //set the preferred sort order and then apply it
        preferredSortOrder = getActivity().getPreferences(Context.MODE_PRIVATE).getInt(SORT_ORDER, 0);

        final NamedColorFragment frag = this;

        mSortOrderBtn = (Button)getView().findViewById(R.id.sort_order_btn);
        mSortOrderBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SortOrderDialogFragment dialog = new SortOrderDialogFragment();
                FragmentManager fm = getFragmentManager();
                dialog.setSortOrder(preferredSortOrder);
                dialog.setTargetFragment(frag, 0);
                dialog.show(fm, ALERT_DIALOG);
            }
        });
    }

    @Override
    public void onStop() {
        SharedPreferences.Editor edit = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
        edit.putInt(SORT_ORDER, preferredSortOrder);
        edit.apply();
        super.onStop();
    }

    /* METHODS
     *=============================*/
    private float[] getHues(String hues){
        String[] pair = hues.split(",");

        float[] result = new float[pair.length];

        for(int i = 0; i < pair.length; i++){
            result[i] = Float.parseFloat(pair[i]);
        }

        return result;
    }

    private HSVGradientDrawable[] getGradients(ColorRecord[] records){
        if(DEBUG) Log.d("NamedColorFragment", "getGradients(ColorRecord[] records)");
        HSVGradientDrawable[] gradients = new HSVGradientDrawable[records.length];
        for(int i = 0; i < records.length; i++){
            int color = Color.HSVToColor(new float[]{records[i].hue, records[i].saturation, records[i].value });
            HSVGradientDrawable gradient = new HSVGradientDrawable( HSVGradientDrawable.Orientation.LEFT_RIGHT, new int[]{ color, color } );
            gradient.setHSV(records[i].hue, records[i].saturation, records[i].value);
            gradients[i] = gradient;
        }
        return gradients;
    }

    private ColorRecord[] getColorRecords(Cursor cursor){
        if(DEBUG) Log.d("NamedColorFragment", "getColorRecords(Cursor cursor)");

        ArrayList<ColorRecord> results = new ArrayList<>();

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            results.add(new ColorRecord(
                    cursor.getString(0),
                    cursor.getFloat(1),
                    cursor.getFloat(2),
                    cursor.getFloat(3))
            );
            cursor.moveToNext();
        }
        cursor.close();

        return (colorRecords = results.toArray(new ColorRecord[results.size()]));
    }

    private Cursor getColors(float startHue, float endHue, float saturation, float value, float delta){
        ContentResolver resolver = getActivity().getContentResolver();

        String[] projection = {
                ColorHelper.COLOR_NAME_COLUMN,
                ColorHelper.HUE_COLUMN,
                ColorHelper.SATURATION_COLUMN,
                ColorHelper.VALUE_COLUMN
        };

        String selection = String.format("(%s BETWEEN ? AND ?) AND (%s BETWEEN ? AND ?) AND (%s BETWEEN ? AND ?)",
                projection[1], projection[2], projection[3]);
        String[] selectionArgs = { startHue+"", endHue+"", (saturation-delta)+"", (saturation+delta)+"", (value-delta)+"", (value+delta)+""};

        return resolver.query(ColorProvider.CONTENT_URI, projection, selection, selectionArgs, null, null );
    }

    public void setPreferredSortOrder(int i){
        if(DEBUG) Log.d("NamedColorFragment", "preferredSortOrder= "+i);
        preferredSortOrder = i;
        applySortOrder(i);
    }

    private void applySortOrder(int i){
        switch(i){
            case 0:
                ((HSVAdapter)mListView.getAdapter()).sort(new HSVComparators.HSVComparator());
                break;

            case 1:
                ((HSVAdapter)mListView.getAdapter()).sort(new HSVComparators.HVSComparator());
                break;

            case 2:
                ((HSVAdapter)mListView.getAdapter()).sort(new HSVComparators.SHVComparator());
                break;

            case 3:
                ((HSVAdapter)mListView.getAdapter()).sort(new HSVComparators.SVHComparator());
                break;

            case 4:
                ((HSVAdapter)mListView.getAdapter()).sort(new HSVComparators.VHSComparator());
                break;

            case 5:
                ((HSVAdapter)mListView.getAdapter()).sort(new HSVComparators.VSHComparator());
                break;
        }
    }
}
