package com.example.faytxzen.colordoctor.models;

import android.graphics.drawable.GradientDrawable;

/**
 * Created by faytxzen on 4/21/15.
 */
public class ColorRecord {
    public String name;
    public float hue, saturation, value;

    public ColorRecord(String name, float hue, float saturation, float value){
        this.name = name;
        this.hue = hue;
        this.saturation = saturation;
        this.value = value;
    }

    public String toString(){
        return String.format("{ name: %s, hue: %s, saturation: %s, value: %s}", name, hue, saturation, value);
    }
}
