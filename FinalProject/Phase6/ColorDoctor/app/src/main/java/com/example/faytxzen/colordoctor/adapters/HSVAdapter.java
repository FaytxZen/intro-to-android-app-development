package com.example.faytxzen.colordoctor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.R;
import com.example.faytxzen.colordoctor.drawables.HSVGradientDrawable;

import java.util.List;

public class HSVAdapter extends ArrayAdapter<HSVGradientDrawable> {
    /* DATA MEMBERS
     *=============================*/
    private ViewHolder viewHolder;
    private Context context;

    /* CONSTRUCTORS
     *=============================*/
    /**
     * @param context
     */
    public HSVAdapter(Context context, HSVGradientDrawable[] gradients) {
        super(context, R.layout.adapter_item, gradients);

        this.context = context;
    }

    /* METHODS
     *=============================*/
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if( convertView == null ){
            viewHolder = new ViewHolder();

            convertView = inflater.inflate(R.layout.adapter_item, parent, false);

            viewHolder.swatch = (TextView)convertView.findViewById(R.id.swatch_item);

            convertView.setTag( viewHolder );
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.swatch.setBackground(getItem(position));

        return convertView;
    }

    private static class ViewHolder {
        TextView swatch;
    }
}
