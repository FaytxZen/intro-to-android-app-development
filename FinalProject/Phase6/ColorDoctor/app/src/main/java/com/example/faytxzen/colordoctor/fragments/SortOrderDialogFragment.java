package com.example.faytxzen.colordoctor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.faytxzen.colordoctor.R;


public class SortOrderDialogFragment extends DialogFragment {
    private static final boolean DEBUG = true;
    private static final CharSequence[] options = {
            "Hue, Saturation, Value", "Hue, Value, Saturation", "Saturation, Hue, Value",
            "Saturation, Value, Hue", "Value, Hue, Saturation", "Value, Saturation, Hue"
    };
    private int sortOrder;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Set the sort order");
        builder.setSingleChoiceItems(options, sortOrder, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(DEBUG) Log.d("SortOrderDialogFragment", "which= "+which);
                ((NamedColorFragment)getTargetFragment()).setPreferredSortOrder(which);
                dialog.dismiss();
            }
        });

        return builder.create();
    }

    public void setSortOrder(int sortOrder){
        this.sortOrder = sortOrder;
    }
}
