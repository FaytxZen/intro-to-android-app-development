package com.example.faytxzen.colordoctor;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.example.faytxzen.colordoctor.database.ColorHelper;
import com.example.faytxzen.colordoctor.fragments.HueFragment;

import java.io.IOException;
import java.io.InputStreamReader;


public class MainActivity extends FragmentActivity {
    public static final boolean DEBUG = true;
    public static final int NUM_ITEMS = 10;
    private final String FRAG_TAG = "COLOR_PICKER_FRAGMENT";
    private Fragment mFragment;
    private FragmentMessage mFragMsg;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager fm = getFragmentManager();
        mFragment = fm.findFragmentByTag(FRAG_TAG);

        if(mFragment == null) {
            mFragment = new HueFragment();
            fm.beginTransaction().add(mFragment, FRAG_TAG).commit();
        }

        startFragment(mFragment);

        //Uncomment for first run only
        /*
        try {
            ColorHelper ch = new ColorHelper(getApplicationContext());
            ch.seedDatabaseWithColors(new InputStreamReader(getAssets().open("wiki_colors.txt")));
        }
        catch(IOException ioe) { ioe.printStackTrace(); }
        */
    }

    /* METHODS
     *=============================*/
    public void startFragment(Fragment fragment){
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.primary_view, fragment, FRAG_TAG).commit();
        mFragment = fragment;
    }

    public void startFragment(Fragment fragment, FragmentMessage msg){
        mFragMsg = msg;
        startFragment(fragment);
    }

    public FragmentMessage getFragmentMessage(){
        return mFragMsg.clone();
    }

    /* Data Objects
     *=============================*/
    public static class FragmentMessage {
        String[] msg;

        public FragmentMessage(String[] msg){
            this.msg = msg;
        }

        public FragmentMessage clone(){
            return new FragmentMessage(msg);
        }

        public String[] getMsg(){
            return msg;
        }
    }
}
