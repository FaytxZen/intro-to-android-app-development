package com.example.faytxzen.colordoctor.contentproviders;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.util.Log;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.database.ColorHelper;

import java.util.Arrays;
import java.util.HashSet;


public class ColorProvider extends ContentProvider {
    private static final boolean DEBUG = MainActivity.DEBUG;
    private ColorHelper colorDb;

    public static final int
            COLORS = 0,
            ID = 1,
            COLOR_NAME = 2,
            HUE = 3,
            SATURATION = 4,
            VALUE = 5;

    private static final String
            AUTHORITY = "com.example.faytxzen.colordoctor.contentproviders",
            BASE_PATH = "colors",
            CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + BASE_PATH,
            CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + BASE_PATH;

    public static final Uri CONTENT_URI = Uri.parse(String.format("content://%s/%s", AUTHORITY, BASE_PATH));

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sUriMatcher.addURI(AUTHORITY, BASE_PATH, COLORS);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", ID);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#/name", COLOR_NAME);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#/hue", HUE);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#/saturation", SATURATION);
        sUriMatcher.addURI(AUTHORITY, BASE_PATH + "/#/value", VALUE);
    }

    /* IMPLEMENTED METHODS
     *=============================*/
    @Override
    public boolean onCreate() {
        if(DEBUG) Log.d("ColorProvider", "OnCreate()");

        colorDb = new ColorHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        if(DEBUG) Log.d("ColorProvider", "query(...)");

        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        //make sure the columns exist
        checkColumns(projection);

        //set the table to query
        qb.setTables(ColorHelper.TABLE_NAME);

        //get the uri type
        int uriType = sUriMatcher.match(uri);

        //append the appropriate columns being checked
        switch(uriType){
            case COLORS:
                break;

            case ID:
                qb.appendWhere(ColorHelper.ID + "=" + uri.getLastPathSegment());
                break;

            case COLOR_NAME:
                qb.appendWhere(ColorHelper.COLOR_NAME_COLUMN + "=" + uri.getLastPathSegment());
                break;

            case HUE:
                qb.appendWhere(ColorHelper.HUE_COLUMN + "=" + uri.getLastPathSegment());
                break;

            case SATURATION:
                qb.appendWhere(ColorHelper.SATURATION_COLUMN + "=" + uri.getLastPathSegment());
                break;

            case VALUE:
                qb.appendWhere(ColorHelper.VALUE_COLUMN + "=" + uri.getLastPathSegment());
                break;

            default:
                throw new IllegalArgumentException("Unknown URI:\t" + uri);
        }

        //query the database and return the cursor
        SQLiteDatabase db = colorDb.getReadableDatabase();
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);
        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if(DEBUG) Log.d("ColorProvider", "insert(...)");

        int uriType = sUriMatcher.match(uri);
        SQLiteDatabase db = colorDb.getWritableDatabase();

        long id = 0;

        //get the next id
        switch( uriType ){
            case COLORS:
                id = db.insert(ColorHelper.TABLE_NAME, null, values);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI:\t"+uri);
        }

        getContext().getContentResolver().notifyChange(uri,null);
        return Uri.parse(BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    /* PRIVATE METHODS
     *=============================*/
    private void checkColumns(String[] projection){
        if(DEBUG) Log.d("ColorProvider", "checkColumns(String[] projection)");

        String[] available = {
                ColorHelper.ID,
                ColorHelper.COLOR_NAME_COLUMN,
                ColorHelper.HUE_COLUMN,
                ColorHelper.SATURATION_COLUMN,
                ColorHelper.VALUE_COLUMN
        };

        if(projection != null){
            HashSet<String> requestedColumns = new HashSet<>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<>(Arrays.asList(available));

            if( !availableColumns.containsAll(requestedColumns)){
                throw new IllegalArgumentException("Unknown columns in projection.");
            }
        }
    }
}
