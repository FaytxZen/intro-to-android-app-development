package com.example.faytxzen.colordoctor.fragments;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.adapters.HSVAdapter;
import com.example.faytxzen.colordoctor.drawables.HSVGradientDrawable;
import com.example.faytxzen.colordoctor.generators.HSVGradients;

/**
 * Created by faytxzen on 4/20/15.
 */
public abstract class BaseHSVFragment extends Fragment {
    /* DATA MEMBERS
     *=============================*/
    protected final String LIST_TAG = "LIST_TAG";
    protected final String CONFIGURE_SWATCH_DIALOG = "CONFIGURE_SWATCH_DIALOG";

    protected int numItems = MainActivity.NUM_ITEMS;
    protected ListView mListView;
    protected TextView mHues, mSaturation, mValue;
    protected Button mSettingsBtn;
    protected float centralHue = 0;

    protected abstract float[] getHues();
    protected abstract float getSaturation();

    /* METHODS
     *=============================*/
    protected void restoreFields(String hues, String saturation, String value){
        mHues.setText(hues);
        mSaturation.setText(saturation);
        mValue.setText(value);
    }

    protected HSVGradientDrawable[] getValues(int state){
        switch(state){
            case 0: //hues
                return HSVGradients.getHSV(numItems, centralHue);

            case 1: //saturation
                float[] hues = getHues();
                return HSVGradients.getHSV(hues[0], hues[1], numItems);

            case 2: //values
                hues = getHues();
                return HSVGradients.getHSV(hues[0], hues[1], getSaturation(), numItems);

            default:
                throw new IllegalArgumentException();
        }
    }

    public void setNumItems(int numItems){
        this.numItems = numItems;
    }

    public void updateListView(int state){
        mListView.setAdapter(new HSVAdapter(getActivity(), getValues(state)));
    }
}
