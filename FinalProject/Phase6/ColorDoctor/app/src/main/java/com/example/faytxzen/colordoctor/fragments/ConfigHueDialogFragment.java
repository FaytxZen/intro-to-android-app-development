package com.example.faytxzen.colordoctor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;

/**
 * Created by faytxzen on 4/21/15.
 */
public class ConfigHueDialogFragment extends DialogFragment {
    private static boolean DEBUG = MainActivity.DEBUG;
    private int currentNumSwatches = MainActivity.NUM_ITEMS;
    private float centralHue;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_seekers, null);
        final TextView curNumSwatches = (TextView)v.findViewById(R.id.num_swatches);
        curNumSwatches.setText(currentNumSwatches + "");

        SeekBar numSwatchSeeker = (SeekBar)v.findViewById(R.id.num_swatches_seekbar);
        numSwatchSeeker.setMax(36);
        numSwatchSeeker.setProgress(currentNumSwatches);
        numSwatchSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress == 0) ++progress;

                currentNumSwatches = progress;
                curNumSwatches.setText(progress + "");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        final TextView centralHueText = (TextView)v.findViewById(R.id.central_hue);
        setTextAndBackground(centralHueText, (int)centralHue);

        if(DEBUG) Log.d("ConfigHueDialogFragment", centralHue+"");
        SeekBar centralHueSeeker = (SeekBar)v.findViewById(R.id.central_hue_seekbar);
        centralHueSeeker.setMax(360);
        centralHueSeeker.setProgress((int)centralHue);
        centralHueSeeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                setTextAndBackground(centralHueText, progress);
                setCentralHue((float)progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        builder.setView(v)
                .setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int state = 0;
                        if(getTargetFragment() instanceof HueFragment) state = 0;

                        ((HueFragment)getTargetFragment()).setCentralHue(centralHue);
                        ((BaseHSVFragment) getTargetFragment()).setNumItems(currentNumSwatches);
                        ((BaseHSVFragment)getTargetFragment()).updateListView(state);

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setTitle("Configure your swatches");

        return builder.create();
    }

    public void setCurNumSwatches(int num){
        currentNumSwatches = num;
    }

    public void setCentralHue(float hue){
        centralHue = hue;
    }

    private void setTextAndBackground(TextView view, int value){
        view.setText(value + "");
        int startColor = Color.HSVToColor(new float[]{ value, 1f, 1f});
        int endColor = Color.HSVToColor(new float[]{ value, 1f, 1f});
        view.setBackground(new GradientDrawable(GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ startColor, endColor }));
    }
}
