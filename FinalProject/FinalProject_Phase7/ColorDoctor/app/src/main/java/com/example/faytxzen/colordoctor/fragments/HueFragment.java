package com.example.faytxzen.colordoctor.fragments;


import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;
import com.example.faytxzen.colordoctor.adapters.HSVAdapter;
import com.example.faytxzen.colordoctor.generators.HSVGradients;


public class HueFragment extends BaseHSVFragment {
    /* DATA MEMBERS
     *=============================*/
    private final int STATE = 0;
    private final boolean DEBUG = MainActivity.DEBUG;
    private final String LIST_TAG = "LIST_TAG";
    private final String NUM_ITEMS_TAG = "NUM_ITEMS_HUE";
    private final String CENTRAL_HUE_TAG = "CENTRAL_HUE_TAG";

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray(LIST_TAG, new String[]{
                mHues.getText().toString(),
                mSaturation.getText().toString(),
                mValue.getText().toString()
        });
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hsv_listview, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //gets the shared preferences for the user
        SharedPreferences pref = getActivity().getPreferences(Context.MODE_PRIVATE);
        numItems = pref.getInt(NUM_ITEMS_TAG, MainActivity.NUM_ITEMS);
        centralHue = pref.getFloat(CENTRAL_HUE_TAG, 0);

        //initialize the bottom readout
        mHues = (TextView) getView().findViewById(R.id.hues_value);
        mSaturation = (TextView) getView().findViewById(R.id.saturation_value);
        mValue = (TextView) getView().findViewById(R.id.values_value);

        //restore the HSV values if any
        if(savedInstanceState != null){
            String[] states = savedInstanceState.getStringArray(LIST_TAG);
            restoreFields(states[0], states[1], states[2]);
        }

        //initialize the settings button
        final HueFragment frag = this;
        mSettingsBtn = (Button) getView().findViewById(R.id.settings_btn);
        mSettingsBtn.setText(R.string.configure_swatch_btn);
        mSettingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfigHueDialogFragment dialog = new ConfigHueDialogFragment();
                dialog.setCurNumSwatches(numItems);
                dialog.setCentralHue(centralHue);
                dialog.setTargetFragment(frag, 0);
                dialog.show(getFragmentManager(), CONFIGURE_SWATCH_DIALOG);
            }
        });

        //initialize the listView
        mListView = (ListView) getView().findViewById(R.id.swatch_list);
        mListView.setAdapter(new HSVAdapter(getActivity(), getValues(STATE)));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setHues(HSVGradients.getStartEndHues(numItems, position));
                MainActivity.FragmentMessage msg = new MainActivity.FragmentMessage(
                        new String[]{mHues.getText().toString()}
                );
                ((MainActivity) getActivity()).startFragment(new SaturationFragment(), msg);
            }
        });
    }

    @Override
    public void onStop(){
        SharedPreferences.Editor edit = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
        edit.putInt(NUM_ITEMS_TAG, numItems);
        edit.putFloat(CENTRAL_HUE_TAG, centralHue);
        edit.apply();
        super.onStop();
    }

    /* METHODS
     *=============================*/
    @Override
    protected float[] getHues(){
        String[] hueStr = mHues.getText().toString().split(",");
        float startHue = Float.parseFloat(hueStr[0]);
        float endHue = Float.parseFloat(hueStr[1]);
        return new float[]{ startHue, endHue };
    }

    /**
     * Gets the saturation of displayed in the mSaturation TextView
     * @return
     */
    @Override
    protected float getSaturation(){
        return Float.parseFloat(mSaturation.getText().toString());
    }

    /**
     * Sets the displayed string for the mHues TextView
     * @param hues
     */
    private void setHues(float[] hues){
        String hueStr = String.format("%s, %s", hues[0], hues[1]);
        mHues.setText(hueStr);
    }

    /**
     * Mutator method for centralHue
     * @param hue
     */
    public void setCentralHue(float hue){
        centralHue = hue;
    }
}
