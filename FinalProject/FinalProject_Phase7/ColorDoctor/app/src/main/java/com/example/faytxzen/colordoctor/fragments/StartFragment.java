package com.example.faytxzen.colordoctor.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;

/**
 * Created by faytxzen on 4/22/15.
 */
public class StartFragment extends Fragment {
    /* DATA MEMBERS
     *=============================*/
    private Button mStartExplorerBtn, mStartIdentifierBtn;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState ) {
        return inflater.inflate(R.layout.fragment_start, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mStartExplorerBtn = (Button)getView().findViewById(R.id.start_explorer_btn);
        mStartExplorerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).startFragment(new HueFragment());
            }
        });

        mStartIdentifierBtn = (Button)getView().findViewById(R.id.start_identify_btn);
        mStartIdentifierBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((MainActivity)getActivity()).startFragment(new IdentifierFragment());
            }
        });
    }
}
