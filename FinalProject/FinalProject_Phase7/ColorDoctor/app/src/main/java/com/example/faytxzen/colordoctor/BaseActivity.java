package com.example.faytxzen.colordoctor;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v4.app.FragmentActivity;

/**
 * Created by faytxzen on 4/22/15.
 */
public abstract class BaseActivity extends FragmentActivity {
    protected FragmentMessage mFragMsg;

    public abstract void startFragment(Fragment fragment);

    public void startFragment(Fragment fragment, FragmentMessage msg){
        mFragMsg = msg;
        startFragment(fragment);
    }

    public FragmentMessage getFragmentMessage(){
        return mFragMsg.clone();
    }

    /* Data Objects
     *=============================*/
    public static class FragmentMessage {
        String[] msg;

        public FragmentMessage(String[] msg){
            this.msg = msg;
        }

        public FragmentMessage clone(){
            return new FragmentMessage(msg);
        }

        public String[] getMsg(){
            return msg;
        }
    }
}
