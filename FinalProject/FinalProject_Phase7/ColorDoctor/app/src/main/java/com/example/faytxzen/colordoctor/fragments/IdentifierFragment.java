package com.example.faytxzen.colordoctor.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;

import com.example.faytxzen.colordoctor.ResultsActivity;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by faytxzen on 4/22/15.
 */
public class IdentifierFragment extends Fragment {
    /* DATA MEMBERS
     *=============================*/
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final String DOMINANT_HSV = "DOMINANT_HSV";

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        sendTakePhotoIntent();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK){
            Bundle extras = data.getExtras();
            float[] dominantHSV = new float[3];
            Color.colorToHSV(findDominantColor((Bitmap)extras.get("data")), dominantHSV);

            startResultsActivity(dominantHSV);
        }
    }

    /* METHODS
     *=============================*/
    /**
     * Starts the ResultsActivity
     * @param hsv
     */
    private void startResultsActivity(float[] hsv){
        Intent intent = new Intent(getActivity(), ResultsActivity.class);
        intent.putExtra(DOMINANT_HSV, hsv);
        getActivity().startActivity(intent);
    }

    /**
     * Finds the mode for the given Bitmap
     * @param img
     * @return
     */
    private int findDominantColor(Bitmap img){
        HashMap<Integer, Integer> counts = new HashMap<>();
        int dominantColor = -1;
        int maxCount = 0;

        for(int i = 0; i < img.getWidth(); i++){
            for(int j = 0; j < img.getHeight(); j++){
                int pixel = img.getPixel(i, j);
                Integer count = counts.get(pixel);
                if(count != null){
                    counts.put(pixel, counts.get(pixel) + 1);
                }
                else {
                    counts.put(pixel, 1);
                }

                count = counts.get(pixel);

                if(count > maxCount){
                    maxCount = count;
                    dominantColor = pixel;
                }
            }
        }

        return dominantColor;
    }

    /**
     * Send an intent with result to the device camera
     */
    private void sendTakePhotoIntent(){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
        }
    }
}
