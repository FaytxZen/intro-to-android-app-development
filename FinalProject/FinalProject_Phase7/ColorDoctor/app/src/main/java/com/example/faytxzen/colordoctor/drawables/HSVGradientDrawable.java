package com.example.faytxzen.colordoctor.drawables;

import android.graphics.drawable.GradientDrawable;

/**
 * Override the regular GradientDrawable to pass additional information around
 */
public class HSVGradientDrawable extends GradientDrawable {
    private float hue, saturation, value;

    public HSVGradientDrawable(Orientation orient, int[] colors){
        super(orient, colors);
    }

    public void setHSV(float hue, float saturation, float value){
        this.hue = hue;
        this.saturation = saturation;
        this.value = value;
    }

    public float[] getHSV(){
        return new float[]{ hue, saturation, value };
    }
}
