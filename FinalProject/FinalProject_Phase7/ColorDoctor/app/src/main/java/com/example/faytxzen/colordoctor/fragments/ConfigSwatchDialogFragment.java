package com.example.faytxzen.colordoctor.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;

/**
 * Created by faytxzen on 4/21/15.
 */
public class ConfigSwatchDialogFragment extends DialogFragment {
    /* DATA MEMBERS
     *=============================*/
    private int currentNumSwatches = MainActivity.NUM_ITEMS;

    /* OVERRIDDEN METHODS
     *=============================*/
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_seeker, null);
        final TextView curNumSwatches = (TextView)v.findViewById(R.id.num_swatches);
        curNumSwatches.setText(currentNumSwatches + "");

        SeekBar seeker = (SeekBar)v.findViewById(R.id.num_swatches_seekbar);
        seeker.setMax(256);
        seeker.setProgress(currentNumSwatches);
        seeker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(progress == 0) ++progress;

                currentNumSwatches = progress;
                curNumSwatches.setText(progress+"");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });

        builder.setView(v)
                .setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int state = 0;
                        if(getTargetFragment() instanceof SaturationFragment) state = 1;
                        else if(getTargetFragment() instanceof ValueFragment) state = 2;

                        ((BaseHSVFragment)getTargetFragment()).setNumItems(currentNumSwatches);
                        ((BaseHSVFragment)getTargetFragment()).updateListView(state);

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setTitle("Set the number of swatches");

        return builder.create();
    }

    /* METHODS
     *=============================*/
    public void setCurNumSwatches(int num){
        currentNumSwatches = num;
    }
}
