package com.example.faytxzen.colordoctor.generators;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.util.Log;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.drawables.HSVGradientDrawable;

import java.util.Arrays;


public class HSVGradients {
    private static final boolean DEBUG = MainActivity.DEBUG;

    /* CONSTRUCTORS
     *=============================*/
    //prevent instantiation and make this a utility class
    private HSVGradients(){}

    /* METHODS
     *=============================*/
    /**
     *
     * @param numChoices
     * @param position
     * @return
     */
    public static float[] getStartEndHues(int numChoices, int position){
        float colorSteps = 360f/(numChoices);
        return new float[] {
                colorSteps * position,
                colorSteps * ++position
        };
    }

    public static float getSaturationOrValue(int numChoices, int position){
        float colorSteps = 1f/numChoices;
        return colorSteps * position;
    }

    public static float getDelta(int numChoices){
        return 1f/numChoices;
    }

    /**
     * Returns an array of gradients with varying hues
     * @param numChoices
     * @return
     */
    public static HSVGradientDrawable[] getHSV(int numChoices, float centralHue){
        HSVGradientDrawable[] result = new HSVGradientDrawable[numChoices];

        float colorSteps = 360f/(numChoices);

        for(int i = 0, j = 0 ; i < numChoices && j < numChoices; j++, i++){
            float pivotHue = (centralHue + colorSteps * i);

            int numIntermediates = 360/(numChoices*2);
            int[] colors = new int[numIntermediates];
            int rising = (int)Math.ceil(numIntermediates / 2);
            int falling = (int)Math.floor(numIntermediates / 2);
            if(rising == falling) rising++;

            for(; rising <= numIntermediates || falling >= 0; rising++, falling--){
                int iteration = (int)(Math.floor(numIntermediates/2) - falling);
                float hueOffset = (colorSteps/2.0f)*iteration/((float)Math.floor(numIntermediates/2));
                float lowerHue = pivotHue - hueOffset;
                float higherHue = pivotHue + hueOffset;

                if(DEBUG) Log.d("HSVGradients", String.format("Iteration is %s, hueOffset is %s", iteration, hueOffset));

                colors[falling] = Color.HSVToColor(new float[]{ lowerHue, 1f, 1f });
                if(rising < numIntermediates) colors[rising] = Color.HSVToColor(new float[]{ higherHue, 1f, 1f });
            }
            result[j] = new HSVGradientDrawable( GradientDrawable.Orientation.LEFT_RIGHT, colors);
        }

        return result;
    }

    /**
     * Returns an array of gradients with varying saturations
     * @param startHue
     * @param endHue
     * @param numChoices
     * @return
     */
    public static HSVGradientDrawable[] getHSV(float startHue, float endHue, int numChoices){
        HSVGradientDrawable[] result = new HSVGradientDrawable[numChoices];

        float colorSteps = 1f/numChoices;

        for(int i = 0 ; i < numChoices ; i++){
            float saturation = colorSteps * i;
            int leftColor = Color.HSVToColor(new float[]{ startHue, saturation, 1f });
            int rightColor = Color.HSVToColor(new float[]{ endHue, saturation, 1f });

            result[i] = new HSVGradientDrawable( GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ leftColor, rightColor } );
        }

        return result;
    }

    /**
     * Returns an array of gradients with varying values
     * @param startHue
     * @param endHue
     * @param saturation
     * @param numChoices
     * @return
     */
    public static HSVGradientDrawable[] getHSV(float startHue, float endHue, float saturation, int numChoices){
        HSVGradientDrawable[] result = new HSVGradientDrawable[numChoices];

        float colorSteps = 1f/numChoices;

        for(int i = 0; i < numChoices; i++){
            float value = colorSteps * i;
            int leftColor = Color.HSVToColor(new float[]{ startHue, saturation, value });
            int rightColor = Color.HSVToColor(new float[]{ endHue, saturation, value });

            result[i] = new HSVGradientDrawable(  GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ leftColor, rightColor } );
        }

        return result;
    }
}
