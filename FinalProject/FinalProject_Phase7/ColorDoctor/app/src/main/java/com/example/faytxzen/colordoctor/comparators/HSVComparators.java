package com.example.faytxzen.colordoctor.comparators;

import com.example.faytxzen.colordoctor.drawables.HSVGradientDrawable;
import com.example.faytxzen.colordoctor.models.ColorRecord;

import java.util.Comparator;

public class HSVComparators {
    //Prevent instantiation as this is just a Comparator utility for HSV values
    private HSVComparators(){}

    /* HSVGRADIENTDRAWABLE COMPARATORS
     *=============================*/
    public static class HSVComparator implements Comparator<HSVGradientDrawable> {
        @Override
        public int compare(HSVGradientDrawable lhs, HSVGradientDrawable rhs) {
            float[] lhsHsv = lhs.getHSV(), rhsHsv = rhs.getHSV();

            int result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result =Float.compare(lhsHsv[2], rhsHsv[2]);

            return result;
        }
    }

    public static class HVSComparator implements Comparator<HSVGradientDrawable> {
        @Override
        public int compare(HSVGradientDrawable lhs, HSVGradientDrawable rhs) {
            float[] lhsHsv = lhs.getHSV(), rhsHsv = rhs.getHSV();

            int result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result =Float.compare(lhsHsv[1], rhsHsv[1]);

            return result;
        }
    }

    public static class SHVComparator implements Comparator<HSVGradientDrawable> {
        @Override
        public int compare(HSVGradientDrawable lhs, HSVGradientDrawable rhs) {
            float[] lhsHsv = lhs.getHSV(), rhsHsv = rhs.getHSV();

            int result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result =Float.compare(lhsHsv[2], rhsHsv[2]);

            return result;
        }
    }

    public static class SVHComparator implements Comparator<HSVGradientDrawable> {
        @Override
        public int compare(HSVGradientDrawable lhs, HSVGradientDrawable rhs) {
            float[] lhsHsv = lhs.getHSV(), rhsHsv = rhs.getHSV();

            int result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result =Float.compare(lhsHsv[0], rhsHsv[0]);

            return result;
        }
    }

    public static class VHSComparator implements Comparator<HSVGradientDrawable> {
        @Override
        public int compare(HSVGradientDrawable lhs, HSVGradientDrawable rhs) {
            float[] lhsHsv = lhs.getHSV(), rhsHsv = rhs.getHSV();

            int result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result =Float.compare(lhsHsv[1], rhsHsv[1]);

            return result;
        }
    }

    public static class VSHComparator implements Comparator<HSVGradientDrawable> {
        @Override
        public int compare(HSVGradientDrawable lhs, HSVGradientDrawable rhs) {
            float[] lhsHsv = lhs.getHSV(), rhsHsv = rhs.getHSV();

            int result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result =Float.compare(lhsHsv[0], rhsHsv[0]);

            return result;
        }
    }

    /* COLOR RECORD COMPARATORS
     *=============================*/
    public static class ColorRecordHSVComparator implements Comparator<ColorRecord> {
        @Override
        public int compare(ColorRecord lhs, ColorRecord rhs) {
            float[] lhsHsv = new float[]{ lhs.hue, lhs.saturation, lhs.value },
                    rhsHsv = new float[]{ rhs.hue, rhs.saturation, rhs.value };

            int result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result =Float.compare(lhsHsv[2], rhsHsv[2]);

            return result;
        }
    }

    public static class ColorRecordHVSComparator implements Comparator<ColorRecord> {
        @Override
        public int compare(ColorRecord lhs, ColorRecord rhs) {
            float[] lhsHsv = new float[]{ lhs.hue, lhs.saturation, lhs.value },
                    rhsHsv = new float[]{ rhs.hue, rhs.saturation, rhs.value };

            int result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result =Float.compare(lhsHsv[1], rhsHsv[1]);

            return result;
        }
    }

    public static class ColorRecordSHVComparator implements Comparator<ColorRecord> {
        @Override
        public int compare(ColorRecord lhs, ColorRecord rhs) {
            float[] lhsHsv = new float[]{ lhs.hue, lhs.saturation, lhs.value },
                    rhsHsv = new float[]{ rhs.hue, rhs.saturation, rhs.value };

            int result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result =Float.compare(lhsHsv[2], rhsHsv[2]);

            return result;
        }
    }

    public static class ColorRecordSVHComparator implements Comparator<ColorRecord> {
        @Override
        public int compare(ColorRecord lhs, ColorRecord rhs) {
            float[] lhsHsv = new float[]{ lhs.hue, lhs.saturation, lhs.value },
                    rhsHsv = new float[]{ rhs.hue, rhs.saturation, rhs.value };

            int result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result =Float.compare(lhsHsv[0], rhsHsv[0]);

            return result;
        }
    }

    public static class ColorRecordVHSComparator implements Comparator<ColorRecord> {
        @Override
        public int compare(ColorRecord lhs, ColorRecord rhs) {
            float[] lhsHsv = new float[]{ lhs.hue, lhs.saturation, lhs.value },
                    rhsHsv = new float[]{ rhs.hue, rhs.saturation, rhs.value };

            int result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result = Float.compare(lhsHsv[0], rhsHsv[0]);
            if(result == 0) result =Float.compare(lhsHsv[1], rhsHsv[1]);

            return result;
        }
    }

    public static class ColorRecordVSHComparator implements Comparator<ColorRecord> {
        @Override
        public int compare(ColorRecord lhs, ColorRecord rhs) {
            float[] lhsHsv = new float[]{ lhs.hue, lhs.saturation, lhs.value },
                    rhsHsv = new float[]{ rhs.hue, rhs.saturation, rhs.value };

            int result = Float.compare(lhsHsv[2], rhsHsv[2]);
            if(result == 0) result = Float.compare(lhsHsv[1], rhsHsv[1]);
            if(result == 0) result =Float.compare(lhsHsv[0], rhsHsv[0]);

            return result;
        }
    }
}


