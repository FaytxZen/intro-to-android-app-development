package com.example.faytxzen.colordoctor;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;

import com.example.faytxzen.colordoctor.database.ColorHelper;
import com.example.faytxzen.colordoctor.fragments.StartFragment;


public class MainActivity extends BaseActivity {
    /* DATA MEMBERS
     *=============================*/
    public static final boolean DEBUG = false;
    public static int NUM_ITEMS = 10;
    private final String FRAG_TAG = "COLOR_PICKER_FRAGMENT";
    private Fragment mFragment;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set up the start fragment
        FragmentManager fm = getFragmentManager();
        mFragment = fm.findFragmentByTag(FRAG_TAG);

        if(mFragment == null) {
            mFragment = new StartFragment();
            fm.beginTransaction().add(mFragment, FRAG_TAG).commit();
        }

        startFragment(mFragment);

        //populate the database if it isn't already populated
        ColorHelper ch = new ColorHelper(getApplicationContext());
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            if(mFragment instanceof StartFragment) return super.onKeyDown(keyCode, event);
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* METHODS
     *=============================*/

    /**
     * Callback method for fragments to start new fragments
     * @param fragment
     */
    public void startFragment(Fragment fragment){
        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.primary_view, fragment, FRAG_TAG).commit();
        mFragment = fragment;
    }
}
