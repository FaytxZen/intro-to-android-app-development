package com.example.faytxzen.colordoctor.fragments;

import android.app.Fragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.R;
import com.example.faytxzen.colordoctor.ResultsActivity;
import com.example.faytxzen.colordoctor.contentproviders.ColorProvider;
import com.example.faytxzen.colordoctor.database.ColorHelper;
import com.example.faytxzen.colordoctor.drawables.HSVGradientDrawable;
import com.example.faytxzen.colordoctor.models.ColorRecord;


public class ColorMatchFragment extends Fragment {
    /* DATA MEMBERS
     *=============================*/
    private TextView mColorDataTxt, mColorSwatch, mResultTxt;
    private ColorRecord mRecord;
    private float[] mHSV;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_color_match, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mColorDataTxt = (TextView)getView().findViewById(R.id.color_data);
        mColorSwatch = (TextView)getView().findViewById(R.id.color_swatch);
        mResultTxt = (TextView)getView().findViewById(R.id.match_result_txt);

        mHSV = ((ResultsActivity)getActivity()).getHSV();

        HSVGradientDrawable gradient = getGradient(getColor(mHSV[0], mHSV[1], mHSV[2]));

        if(gradient != null){
            mColorSwatch.setBackground(gradient);
            if(mRecord != null) mColorDataTxt.setText(String.format("Name: %s, Hue: %s, Saturation: %s, Value: %s",
                    mRecord.name, mRecord.hue, mRecord.saturation, mRecord.value ));
            mResultTxt.setText(R.string.exact_match_txt);
        }
        else {
            mResultTxt.setText(R.string.no_match_txt);
        }
    }

    /* METHODS
     *=============================*/

    /**
     * Gets the gradient for a given cursor of the colors schema
     * @param cursor
     * @return
     */
    private HSVGradientDrawable getGradient(Cursor cursor){
        HSVGradientDrawable gradient = null;

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            mRecord = new ColorRecord(cursor.getString(0),cursor.getFloat(1), cursor.getFloat(2), cursor.getFloat(3));

            int color = Color.HSVToColor(new float[]{ cursor.getFloat(1), cursor.getFloat(2), cursor.getFloat(3) });
            gradient = new HSVGradientDrawable( HSVGradientDrawable.Orientation.LEFT_RIGHT, new int[]{ color, color } );

            cursor.moveToNext();
        }
        cursor.close();

        return gradient;
    }

    /**
     * Gets the cursor for an exact match to the given values
     * @param hue
     * @param saturation
     * @param value
     * @return
     */
    private Cursor getColor(float hue, float saturation, float value){
        ContentResolver resolver = getActivity().getContentResolver();

        String[] projection = {
                ColorHelper.COLOR_NAME_COLUMN,
                ColorHelper.HUE_COLUMN,
                ColorHelper.SATURATION_COLUMN,
                ColorHelper.VALUE_COLUMN
        };

        String selection = String.format("(%s = ?) AND (%s = ?) AND (%s = ?)",
                projection[1], projection[2], projection[3]);
        String[] selectionArgs = { hue+"", saturation+"", value+""};

        return resolver.query(ColorProvider.CONTENT_URI, projection, selection, selectionArgs, null, null );
    }
}
