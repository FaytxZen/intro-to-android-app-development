package com.example.faytxzen.colordoctor;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.example.faytxzen.colordoctor.fragments.ColorMatchFragment;
import com.example.faytxzen.colordoctor.fragments.IdentifierFragment;
import com.example.faytxzen.colordoctor.fragments.NamedColorFragment;
import com.example.faytxzen.colordoctor.fragments.StartFragment;

/**
 * Created by faytxzen on 4/22/15.
 */
public class ResultsActivity extends BaseActivity {
    /* DATA MEMBERS
     *=============================*/
    private static final boolean DEBUG = MainActivity.DEBUG;
    private static final String EXACT_MATCH_FRAGMENT = "EXACT_MATCH_FRAGMENT";
    private static final String NAMED_COLOR_FRAGMENT = "NAMED_COLOR_FRAGMENT";
    private Fragment mExactMatchFragment, mNamedColorsFragment;
    private float[] mHSV;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        if(DEBUG) Log.d("ResultsActivity","onCreate(Bundle savedInstanceState)");

        //look in the usual places for HSV data and set the mFragMsg member for use in NamedColorFragment
        Intent intent = getIntent();
        if(intent != null){
            mHSV = intent.getFloatArrayExtra(IdentifierFragment.DOMINANT_HSV);
        }

        String[] hsvStr = new String[3];
        float hueDelta = mHSV[0] * 0.25f; //make the delta take in hues within 25%
        MainActivity.NUM_ITEMS = 5;
        for(int i = 0; i < mHSV.length; i++){
            hsvStr[0] = String.format("%s, %s", mHSV[0] - hueDelta, mHSV[0] + hueDelta);
            hsvStr[1] = mHSV[1]+"";
            hsvStr[2] = mHSV[2]+"";
        }
        mFragMsg = new FragmentMessage(hsvStr);

        //start the fragments
        FragmentManager fm = getFragmentManager();
        mExactMatchFragment = fm.findFragmentByTag(EXACT_MATCH_FRAGMENT);
        mNamedColorsFragment = fm.findFragmentByTag(NAMED_COLOR_FRAGMENT);

        if(mExactMatchFragment == null) {
            mExactMatchFragment = new ColorMatchFragment();
            fm.beginTransaction().replace(R.id.primary_view, mExactMatchFragment, EXACT_MATCH_FRAGMENT).commit();
        }
        if(mNamedColorsFragment == null){
            mNamedColorsFragment = new NamedColorFragment();
            fm.beginTransaction().replace(R.id.secondary_view, mNamedColorsFragment, NAMED_COLOR_FRAGMENT).commit();
        }
    }

    @Override
    public void startFragment(Fragment fragment) {}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK ) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /* METHODS
     *=============================*/
    /**
     * Accessor method for mHSV
     * @return
     */
    public float[] getHSV(){
        return mHSV;
    }
}
