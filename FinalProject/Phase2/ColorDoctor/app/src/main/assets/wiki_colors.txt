Acid green, 65, 86, 75
Aero, 206, 47, 91
Aero blue, 151, 21, 100
African violet, 288, 31, 75
Air Force blue (RAF), 204, 45, 66
Air Force blue (USAF), 220, 100, 56
Air superiority blue, 205, 41, 76
Alabama crimson, 346, 100, 69
Alice blue, 208, 6, 100
Red crimson, 355, 83, 89
Alloy orange, 27, 92, 77
Almond, 30, 14, 94
Amaranth, 348, 81, 90
Amaranth deep purple, 342, 77, 67
Amaranth pink, 338, 35, 95
Amaranth purple, 342, 77, 67
Amaranth red, 356, 84, 83
Amazon, 147, 52, 48
Amber, 45, 100, 100
Amber (SAE/ECE), 30, 100, 100
American rose, 346, 99, 100
Amethyst, 270, 50, 80
Android green, 74, 71, 78
Anti-flash white, 210, 1, 96
Antique brass, 22, 43, 80
Antique bronze, 53, 71, 40
Antique fuchsia, 316, 37, 57
Antique ruby, 350, 80, 52
Antique white, 34, 14, 98
Ao (English), 120, 100, 50
Apple green, 74, 100, 71
Apricot, 24, 29, 98
Aqua, 180, 100, 100
Aquamarine, 160, 50, 100
Arctic lime, 72, 92, 100
Army green, 69, 61, 33
Arsenic, 206, 21, 29
Artichoke, 76, 20, 59
Arylide yellow, 51, 54, 91
Ash grey, 135, 6, 75
Asparagus, 93, 37, 66
Atomic tangerine, 20, 60, 100
Auburn, 0, 75, 65
Aureolin, 56, 100, 99
AuroMetalSaurus, 183, 14, 50
Avocado, 81, 98, 51
Azure, 210, 100, 100
Azure (web color), 180, 6, 100
Azure mist, 180, 6, 100
Azureish white, 206, 10, 96
Baby blue, 199, 43, 94
Baby blue eyes, 209, 33, 95
Baby pink, 0, 20, 96
Baby powder, 60, 2, 100
Baker-Miller pink, 344, 43, 100
Ball blue, 192, 84, 80
Banana Mania, 43, 28, 98
Banana yellow, 51, 79, 100
Bangladesh green, 164, 100, 42
Barbie pink, 327, 85, 88
Barn red, 4, 98, 49
Battleship grey, 60, 2, 52
Bazaar, 353, 22, 60
Beau blue, 206, 18, 90
Beaver, 22, 30, 62
Beige, 60, 10, 96
B'dazzled blue, 215, 69, 58
Big dip o’ruby, 345, 76, 61
Bisque, 33, 23, 100
Bistre, 24, 49, 24
Bistre brown, 43, 85, 59
Bitter lemon, 66, 94, 88
Bitter lime, 75, 100, 100
Bittersweet, 6, 63, 100
Bittersweet shimmer, 359, 59, 75
Black, 0, 0, 0
Black bean, 10, 97, 24
Black leather jacket, 135, 30, 21
Black olive, 70, 10, 24
Blanched almond, 36, 20, 100
Blast-off bronze, 12, 39, 65
Bleu de France, 210, 79, 91
Blizzard Blue, 188, 28, 93
Blond, 50, 24, 98
Blue, 240, 100, 100
Blue (Crayola), 217, 88, 100
Blue (Munsell), 190, 100, 69
Blue (NCS), 197, 100, 74
Blue (Pantone), 231, 100, 66
Blue (pigment), 240, 67, 60
Blue (RYB), 224, 99, 100
Blue Bell, 240, 22, 82
Blue-gray, 210, 50, 80
Blue-green, 192, 93, 73
Blue Lagoon, 193, 41, 63
Blue-magenta violet, 261, 64, 57
Blue sapphire, 197, 86, 50
Blue-violet, 271, 81, 89
Blue yonder, 217, 52, 65
Blueberry, 220, 68, 97
Bluebonnet, 240, 88, 94
Blush, 342, 58, 87
Bole, 9, 51, 47
Bondi blue, 191, 100, 71
Bone, 39, 11, 89
Boston University Red, 0, 100, 80
Bottle green, 164, 100, 42
Boysenberry, 328, 63, 53
Brandeis blue, 214, 100, 100
Brass, 52, 64, 71
Brick red, 352, 68, 80
Bright cerulean, 194, 86, 84
Bright green, 96, 100, 100
Bright lavender, 272, 35, 89
Bright lilac, 285, 39, 94
Bright maroon, 346, 83, 76
Bright navy blue, 210, 88, 82
Bright pink, 330, 100, 100
Bright turquoise, 177, 97, 91
Bright ube, 281, 31, 91
Brilliant azure, 210, 80, 100
Brilliant lavender, 290, 27, 100
Brilliant rose, 332, 67, 100
Brink pink, 348, 62, 98
British racing green, 154, 100, 26
Bronze, 30, 76, 80
Bronze Yellow, 58, 100, 45
Brown (traditional), 30, 100, 59
Brown (web), 0, 75, 65
Brown-nose, 28, 67, 42
Brown Yellow, 30, 50, 80
Brunswick green, 162, 65, 30
Bubble gum, 349, 24, 100
Bubbles, 183, 9, 100
Buff, 49, 46, 94
Bud green, 102, 47, 71
Bulgarian rose, 359, 92, 28
Burgundy, 345, 100, 50
Burlywood, 34, 39, 87
Burnt orange, 25, 100, 80
Burnt sienna, 14, 65, 91
Burnt umber, 9, 74, 54
Byzantine, 311, 73, 74
Byzantium, 311, 63, 44
Cadet, 199, 27, 45
Cadet blue, 182, 41, 63
Cadet grey, 205, 18, 69
Cadmium green, 154, 100, 42
Cadmium orange, 28, 81, 93
Cadmium red, 351, 100, 89
Cadmium yellow, 58, 100, 100
Café au lait, 26, 45, 65
Café noir, 30, 56, 29
Cal Poly green, 137, 61, 30
Cambridge Blue, 140, 16, 76
Camel, 33, 45, 76
Cameo pink, 340, 22, 94
Camouflage green, 91, 20, 53
Canary yellow, 56, 100, 100
Candy apple red, 2, 100, 100
Candy pink, 355, 50, 89
Capri, 195, 100, 100
Caput mortuum, 7, 64, 35
Cardinal, 350, 85, 77
Caribbean green, 165, 100, 80
Carmine, 350, 100, 59
Carmine (M&P), 342, 100, 84
Carmine pink, 4, 72, 92
Carmine red, 347, 100, 100
Carnation pink, 336, 35, 100
Carnelian, 0, 85, 70
Carolina blue, 204, 59, 83
Carrot orange, 33, 86, 93
Castleton green, 164, 100, 34
Catalina blue, 221, 95, 47
Catawba, 348, 52, 44
Cedar Chest, 8, 64, 79
Ceil, 225, 29, 81
Celadon, 123, 24, 88
Celadon blue, 196, 100, 65
Celadon green, 174, 64, 52
Celeste, 180, 30, 100
Celestial blue, 205, 65, 82
Cerise, 343, 78, 87
Cerise pink, 336, 75, 93
Cerulean, 196, 100, 65
Cerulean blue, 224, 78, 75
Cerulean frost, 208, 44, 76
CG Blue, 196, 100, 65
CG Red, 4, 78, 88
Chamoisee, 26, 44, 63
Champagne, 37, 17, 97
Charcoal, 204, 32, 31
Charleston green, 180, 19, 17
Charm pink, 340, 38, 90
Chartreuse (traditional), 68, 100, 100
Chartreuse (web), 90, 100, 100
Cherry, 343, 78, 87
Cherry blossom pink, 348, 28, 100
Chestnut, 10, 64, 58
China pink, 333, 50, 87
China rose, 340, 52, 66
Chinese red, 11, 82, 67
Chinese violet, 296, 29, 53
Chocolate (traditional), 31, 100, 48
Chocolate (web), 25, 86, 82
Chrome yellow, 39, 100, 100
Cinereous, 12, 19, 60
Cinnabar, 5, 77, 89
Cinnamon, 25, 86, 82
Citrine, 54, 96, 89
Citron, 64, 82, 66
Claret, 343, 82, 50
Classic rose, 326, 19, 98
Cobalt Blue, 215, 100, 67
Cocoa brown, 25, 86, 82
Coconut, 19, 59, 59
Coffee, 25, 50, 44
Columbia Blue, 200, 13, 89
Congo pink, 5, 51, 97
Cool Black, 212, 100, 38
Cool grey, 229, 19, 67
Copper, 29, 72, 72
Copper (Crayola), 18, 53, 85
Copper penny, 5, 39, 68
Copper red, 14, 60, 80
Copper rose, 0, 33, 60
Coquelicot, 13, 100, 100
Coral, 16, 69, 100
Coral pink, 5, 51, 97
Coral red, 0, 75, 100
Cordovan, 355, 54, 54
Corn, 54, 63, 98
Cornell Red, 0, 85, 70
Cornflower blue, 219, 58, 93
Cornsilk, 48, 14, 100
Cosmic latte, 43, 9, 100
Coyote brown, 62, 52, 51
Cotton candy, 334, 26, 100
Cream, 57, 18, 100
Crimson, 348, 91, 86
Crimson glory, 344, 100, 75
Crimson red, 0, 100, 60
Cyan, 180, 100, 100
Cyan azure, 209, 57, 71
Cyan-blue azure, 210, 63, 75
Cyan cobalt blue, 215, 74, 61
Cyan cornflower blue, 199, 88, 76
Cyan (process), 193, 100, 92
Cyber grape, 263, 47, 49
Cyber yellow, 50, 100, 100
Daffodil, 60, 81, 100
Dandelion, 55, 80, 94
Dark blue, 240, 100, 55
Dark blue-gray, 240, 33, 60
Dark brown, 30, 67, 40
Dark brown-tangelo, 24, 43, 53
Dark byzantium, 315, 39, 36
Dark candy apple red, 0, 100, 64
Dark cerulean, 209, 94, 49
Dark chestnut, 10, 37, 60
Dark coral, 10, 66, 80
Dark cyan, 180, 100, 55
Dark electric blue, 206, 31, 47
Dark goldenrod, 43, 94, 72
Dark gray (X11), 0, 0, 66
Dark green, 158, 98, 20
Dark green (X11), 120, 100, 39
Dark gunmetal, 202, 26, 16
Dark imperial blue, 203, 100, 42
Dark imperial blue, 183, 100, 40
Dark jungle green, 162, 28, 14
Dark khaki, 56, 43, 74
Dark lava, 27, 31, 28
Dark lavender, 270, 47, 59
Dark liver, 330, 10, 33
Dark liver (horses), 12, 35, 33
Dark magenta, 300, 100, 55
Dark medium gray, 0, 0, 66
Dark midnight blue, 210, 100, 40
Dark moss green, 80, 62, 36
Dark olive green, 82, 56, 42
Dark orange, 33, 100, 100
Dark orchid, 280, 75, 80
Dark pastel blue, 212, 41, 80
Dark pastel green, 138, 98, 75
Dark pastel purple, 263, 48, 84
Dark pastel red, 9, 82, 76
Dark pink, 342, 64, 91
Dark powder blue, 220, 100, 60
Dark puce, 354, 27, 31
Dark purple, 291, 51, 20
Dark raspberry, 330, 72, 53
Dark red, 0, 100, 55
Dark salmon, 15, 48, 91
Dark scarlet, 344, 97, 34
Dark sea green, 120, 24, 74
Dark sienna, 0, 67, 24
Dark sky blue, 199, 35, 84
Dark slate blue, 248, 56, 55
Dark slate gray, 180, 41, 31
Dark spring green, 150, 80, 45
Dark tan, 45, 44, 57
Dark tangerine, 38, 93, 100
Dark taupe, 27, 31, 28
Dark terra cotta, 353, 62, 80
Dark turquoise, 181, 100, 82
Dark vanilla, 32, 20, 82
Dark violet, 282, 100, 83
Dark yellow, 52, 92, 61
Dartmouth green, 152, 100, 44
Davy's grey, 0, 0, 33
Debian red, 339, 95, 84
Deep aquamarine, 161, 51, 51
Deep carmine, 347, 81, 66
Deep carmine pink, 357, 80, 94
Deep carrot orange, 19, 81, 91
Deep cerise, 330, 77, 85
Deep champagne, 35, 34, 98
Deep chestnut, 3, 61, 73
Deep coffee, 1, 42, 44
Deep fuchsia, 300, 56, 76
Deep Green, 122, 95, 40
Deep green-cyan turquoise, 165, 89, 49
Deep jungle green, 178, 100, 29
Deep koamaru, 240, 50, 40
Deep lemon, 47, 89, 96
Deep lilac, 280, 55, 73
Deep magenta, 300, 100, 80
Deep maroon, 0, 100, 51
Deep mauve, 300, 46, 83
Deep moss green, 129, 44, 37
Deep peach, 26, 36, 100
Deep pink, 328, 92, 100
Deep puce, 351, 46, 66
Deep Red, 0, 99, 52
Deep ruby, 336, 52, 52
Deep saffron, 30, 80, 100
Deep sky blue, 195, 100, 100
Deep Space Sparkle, 194, 31, 42
Deep spring bud, 82, 56, 42
Deep Taupe, 356, 25, 49
Deep Tuscan red, 342, 35, 40
Deep violet, 270, 100, 40
Deer, 28, 52, 73
Denim, 213, 89, 74
Desaturated cyan, 180, 33, 60
Desert, 33, 45, 76
Desert sand, 25, 26, 93
Desire, 352, 74, 92
Diamond, 191, 27, 100
Dim gray, 0, 0, 41
Dirt, 29, 46, 61
Dodger blue, 210, 88, 100
Dogwood rose, 335, 89, 84
Dollar bill, 98, 46, 73
Donkey brown, 35, 61, 40
Drab, 43, 85, 59
Duke blue, 240, 100, 61
Dust storm, 6, 12, 90
Dutch white, 42, 22, 94
Earth yellow, 34, 58, 88
Ebony, 97, 14, 36
Ecru, 45, 34, 76
Eerie black, 0, 0, 11
Eggplant, 329, 34, 38
Eggshell, 46, 11, 94
Egyptian blue, 226, 90, 65
Electric blue, 183, 51, 100
Electric crimson, 345, 100, 100
Electric cyan, 180, 100, 100
Electric green, 120, 100, 100
Electric indigo, 266, 100, 100
Electric lavender, 290, 27, 100
Electric lime, 72, 100, 100
Electric purple, 285, 100, 100
Electric ultramarine, 255, 100, 100
Electric violet, 274, 100, 100
Electric yellow, 60, 80, 100
Emerald, 140, 60, 78
Eminence, 284, 63, 51
English green, 162, 65, 30
English lavender, 338, 27, 71
English red, 356, 56, 67
English violet, 289, 35, 36
Eton blue, 134, 25, 78
Eucalyptus, 161, 68, 84
Fallow, 33, 45, 76
Falu red, 0, 81, 50
Fandango, 320, 72, 71
Fandango pink, 338, 63, 87
Fashion fuchsia, 320, 100, 96
Fawn, 30, 51, 90
Feldgrau, 143, 17, 36
Feldspar, 28, 30, 99
Fern green, 106, 45, 47
Ferrari Red, 9, 100, 100
Field drab, 42, 72, 42
Firebrick, 0, 81, 70
Fire engine red, 357, 84, 81
Flame, 17, 85, 89
Flamingo pink, 344, 44, 99
Flattery, 28, 67, 42
Flavescent, 52, 43, 97
Flax, 50, 45, 93
Flirt, 320, 100, 64
Floral white, 40, 6, 100
Fluorescent orange, 45, 100, 100
Fluorescent pink, 328, 92, 100
Fluorescent yellow, 72, 100, 100
Folly, 341, 100, 100
Forest green (traditional), 149, 99, 27
Forest green (web), 120, 76, 55
French beige, 26, 45, 65
French bistre, 34, 42, 52
French blue, 203, 100, 73
French fuchsia, 334, 75, 99
French lilac, 290, 32, 56
French lime, 89, 78, 99
French mauve, 300, 46, 83
French pink, 339, 57, 99
French plum, 325, 84, 51
French puce, 11, 88, 31
French raspberry, 349, 78, 78
French rose, 338, 70, 96
French sky blue, 212, 53, 100
French violet, 279, 97, 81
French wine, 344, 83, 67
Fresh Air, 196, 35, 100
Fuchsia, 300, 100, 100
Fuchsia (Crayola), 300, 56, 76
Fuchsia pink, 300, 53, 100
Fuchsia purple, 333, 72, 80
Fuchsia rose, 337, 66, 78
Fulvous, 35, 100, 89
Fuzzy Wuzzy, 0, 50, 80
Gainsboro, 0, 0, 86
Gamboge, 39, 93, 89
Gamboge orange (brown), 40, 100, 60
Generic viridian, 168, 100, 50
Ghost white, 240, 3, 100
Giants orange, 16, 89, 100
Glaucous, 216, 47, 71
Glitter, 234, 8, 98
GO green, 156, 100, 67
Gold (metallic), 46, 74, 83
Gold (web) (Golden), 51, 100, 100
Gold Fusion, 43, 41, 52
Golden brown, 36, 86, 60
Golden poppy, 46, 100, 99
Golden yellow, 52, 100, 100
Goldenrod, 43, 85, 85
Granny Smith Apple, 113, 30, 89
Grape, 272, 73, 66
Grussrel, 34, 100, 69
Gray, 0, 0, 50
Gray (HTML/CSS gray), 0, 0, 50
Gray (X11 gray), 0, 0, 75
Gray-asparagus, 117, 22, 35
Gray-blue, 229, 19, 67
Green (Color Wheel) (X11 green), 120, 100, 100
Green (Crayola), 158, 84, 67
Green (HTML/CSS color), 120, 100, 50
Green (Munsell), 163, 100, 66
Green (NCS), 160, 100, 62
Green (Pantone), 143, 100, 68
Green (pigment), 149, 100, 65
Green (RYB), 95, 72, 69
Green-blue, 209, 91, 71
Green-cyan, 160, 100, 60
Green-yellow, 84, 82, 100
Grizzly, 34, 82, 53
Grullo, 34, 21, 66
Guppie green, 150, 100, 100
Gunmetal, 200, 15, 19
Halayà úbe, 323, 45, 40
Han blue, 223, 67, 81
Han purple, 255, 90, 98
Hansa yellow, 51, 54, 91
Harlequin, 105, 100, 100
Harlequin green, 105, 88, 80
Harvard crimson, 353, 100, 79
Harvest gold, 40, 100, 85
Heart Gold, 60, 100, 50
Heliotrope, 286, 55, 100
Heliotrope gray, 303, 11, 67
Heliotrope magenta, 295, 100, 73
Hollywood cerise, 320, 100, 96
Honeydew, 120, 6, 100
Honolulu blue, 203, 100, 69
Hooker's green, 163, 40, 47
Hot magenta, 313, 89, 100
Hot pink, 330, 59, 100
Hunter green, 129, 44, 37
Iceberg, 207, 46, 82
Icterine, 58, 63, 99
Illuminating Emerald, 164, 66, 57
Imperial, 289, 56, 42
Imperial blue, 226, 100, 58
Imperial purple, 325, 98, 40
Imperial red, 355, 83, 93
Inchworm, 84, 61, 93
Independence, 231, 30, 43
India green, 115, 94, 53
Indian red, 0, 55, 80
Indian yellow, 35, 62, 89
Indigo, 266, 100, 100
Indigo dye, 230, 94, 57
Indigo (web), 275, 100, 51
International Klein Blue, 223, 100, 65
International orange (aerospace), 19, 100, 100
International orange (engineering), 3, 94, 73
International orange (Golden Gate Bridge), 4, 77, 75
Iris, 245, 62, 81
Irresistible, 338, 62, 70
Isabelline, 30, 3, 96
Islamic green, 120, 100, 56
Italian sky blue, 180, 30, 100
Ivory, 60, 6, 100
Jade, 158, 100, 66
Japanese carmine, 355, 74, 62
Japanese indigo, 189, 47, 28
Japanese violet, 307, 45, 36
Jasmine, 47, 49, 97
Jasper, 359, 73, 84
Jazzberry jam, 328, 93, 65
Jelly Bean, 8, 64, 85
Jet, 0, 0, 20
Jonquil, 49, 91, 96
Jordy blue, 213, 43, 95
June bud, 73, 60, 85
Jungle green, 163, 76, 67
Kelly green, 101, 88, 73
Kenyan copper, 12, 96, 49
Keppel, 171, 67, 69
Khaki (HTML/CSS) (Khaki), 37, 26, 76
Khaki (X11) (Light khaki), 54, 42, 94
Kobe, 12, 83, 53
Kobi, 329, 31, 91
Kobicha, 28, 67, 42
Kombu green, 103, 27, 26
KU Crimson, 357, 100, 91
La Salle Green, 141, 93, 47
Languid lavender, 278, 9, 87
Lapis lazuli, 210, 76, 61
Laser Lemon, 60, 60, 100
Laurel green, 95, 16, 73
Lava, 355, 92, 81
Lavender (floral), 275, 43, 86
Lavender (web), 240, 8, 98
Lavender blue, 240, 20, 100
Lavender blush, 340, 6, 100
Lavender gray, 245, 6, 82
Lavender indigo, 265, 63, 92
Lavender magenta, 300, 45, 93
Lavender mist, 240, 8, 98
Lavender pink, 332, 31, 98
Lavender purple, 267, 32, 71
Lavender rose, 316, 36, 98
Lawn green, 90, 100, 99
Lemon, 58, 100, 100
Lemon chiffon, 54, 20, 100
Lemon curry, 45, 86, 80
Lemon glacier, 60, 100, 100
Lemon lime, 67, 100, 100
Lemon meringue, 47, 23, 96
Lemon yellow, 56, 69, 100
Lenurple, 274, 32, 85
Licorice, 6, 38, 10
Liberty, 236, 50, 65
Light apricot, 28, 30, 99
Light blue, 195, 25, 90
Light brilliant red, 0, 82, 100
Light brown, 28, 84, 71
Light carmine pink, 355, 55, 90
Light cobalt blue, 215, 39, 88
Light coral, 0, 47, 94
Light cornflower blue, 201, 37, 92
Light crimson, 343, 57, 96
Light cyan, 180, 12, 100
Light deep pink, 318, 64, 100
Light French beige, 38, 37, 78
Light fuchsia pink, 305, 47, 98
Light goldenrod yellow, 60, 16, 98
Light gray, 0, 0, 83
Light grayish magenta, 300, 25, 80
Light green, 120, 39, 93
Light hot pink, 326, 30, 100
Light khaki, 54, 42, 94
Light medium orchid, 309, 27, 83
Light moss green, 120, 22, 87
Light orchid, 315, 27, 90
Light pastel purple, 261, 28, 85
Light pink, 351, 29, 100
Light red ochre, 14, 65, 91
Light salmon, 17, 52, 100
Light salmon pink, 0, 40, 100
Light sea green, 177, 82, 70
Light sky blue, 203, 46, 98
Light slate gray, 210, 22, 60
Light steel blue, 214, 21, 87
Light taupe, 26, 39, 70
Light Thulian pink, 340, 38, 90
Light yellow, 60, 12, 100
Lilac, 300, 19, 78
Lime (color wheel), 75, 100, 100
Lime (web) (X11 green), 120, 100, 100
Lime green, 120, 76, 80
Limerick, 72, 95, 76
Lincoln green, 106, 94, 35
Linen, 30, 8, 98
Lion, 33, 45, 76
Liseran Purple, 333, 50, 87
Little boy blue, 212, 51, 86
Liver, 9, 31, 40
Liver (dogs), 29, 78, 72
Liver (organ), 12, 71, 42
Liver chestnut, 27, 43, 60
Livid, 210, 50, 80
Lumber, 28, 20, 100
Lust, 0, 86, 90
Macaroni and Cheese, 27, 47, 100
Magenta, 300, 100, 100
Magenta (Crayola), 332, 67, 100
Magenta (dye), 328, 85, 79
Magenta (Pantone), 334, 69, 82
Magenta (process), 326, 100, 100
Magenta haze, 327, 57, 62
Magenta-pink, 325, 75, 80
Magic mint, 153, 29, 94
Magnolia, 262, 4, 100
Mahogany, 20, 100, 75
Maize, 54, 63, 98
Majorelle Blue, 247, 64, 86
Malachite, 140, 95, 85
Manatee, 231, 11, 67
Mango Tango, 20, 74, 100
Mantis, 110, 48, 76
Mardi Gras, 301, 100, 53
Marigold, 39, 85, 91
Maroon (Crayola), 346, 83, 76
Maroon (HTML/CSS), 0, 100, 50
Maroon (X11), 338, 73, 69
Mauve, 276, 31, 100
Mauve taupe, 343, 34, 57
Mauvelous, 348, 36, 94
May green, 112, 55, 57
Maya blue, 205, 54, 98
Meat brown, 44, 74, 90
Medium aquamarine, 154, 54, 87
Medium blue, 240, 100, 80
Medium candy apple red, 350, 97, 89
Medium carmine, 5, 70, 69
Medium champagne, 48, 30, 95
Medium electric blue, 209, 98, 59
Medium jungle green, 161, 47, 21
Medium lavender magenta, 300, 28, 87
Medium orchid, 288, 60, 83
Medium Persian blue, 203, 100, 65
Medium purple, 260, 49, 86
Medium red-violet, 324, 73, 73
Medium ruby, 337, 62, 67
Medium sea green, 147, 66, 70
Medium sky blue, 190, 46, 92
Medium slate blue, 249, 56, 93
Medium spring bud, 73, 39, 86
Medium spring green, 157, 100, 98
Medium taupe, 9, 31, 40
Medium turquoise, 178, 66, 82
Medium Tuscan red, 9, 51, 47
Medium vermilion, 14, 73, 85
Medium violet-red, 322, 89, 78
Mellow apricot, 30, 52, 97
Mellow yellow, 47, 49, 97
Melon, 7, 29, 99
Metallic Seaweed, 186, 93, 55
Metallic Sunburst, 41, 64, 61
Mexican pink, 327, 100, 89
Midnight blue, 240, 78, 44
Midnight green (eagle green), 187, 100, 33
Mikado yellow, 45, 95, 100
Mindaro, 72, 45, 98
Ming, 188, 56, 49
Mint, 158, 66, 71
Mint cream, 150, 4, 100
Mint green, 120, 40, 100
Misty rose, 6, 12, 100
Moccasin, 34, 14, 98
Mode beige, 43, 85, 59
Moonstone blue, 199, 41, 76
Mordant red 19, 4, 100, 68
Moss green, 75, 41, 60
Mountain Meadow, 161, 74, 73
Mountbatten pink, 323, 20, 60
MSU Green, 167, 65, 27
Mughal green, 120, 50, 38
Mulberry, 328, 62, 77
Mustard, 47, 65, 100
Myrtle green, 176, 59, 47
Nadeshiko pink, 339, 30, 96
Napier green, 100, 100, 50
Naples yellow, 48, 62, 98
Navajo white, 36, 32, 100
Navy, 240, 100, 50
Navy purple, 265, 63, 92
Neon Carrot, 31, 74, 100
Neon fuchsia, 349, 74, 100
Neon green, 111, 92, 100
New Car, 223, 83, 78
New York pink, 3, 41, 84
Non-photo blue, 193, 31, 93
North Texas Green, 140, 97, 56
Nyanza, 97, 14, 100
Ocean Boat Blue, 202, 100, 75
Ochre, 30, 83, 80
Office green, 120, 100, 50
Old burgundy, 6, 31, 26
Old gold, 49, 71, 81
Old heliotrope, 289, 35, 36
Old lace, 39, 9, 99
Old lavender, 304, 14, 47
Old mauve, 336, 52, 40
Old moss green, 54, 60, 53
Old rose, 359, 33, 75
Old silver, 60, 2, 52
Olive, 60, 100, 50
Olive Drab (#3), 80, 75, 56
Olive Drab #7, 43, 48, 24
Olivine, 87, 38, 73
Onyx, 195, 7, 22
Opera mauve, 319, 28, 72
Orange (color wheel), 30, 100, 100
Orange (Crayola), 18, 78, 100
Orange (Pantone), 21, 100, 100
Orange (RYB), 36, 99, 98
Orange (web), 39, 100, 100
Orange peel, 37, 100, 100
Orange-red, 16, 100, 100
Orange-yellow, 45, 58, 97
Orchid, 302, 49, 85
Orchid pink, 342, 22, 95
Orioles orange, 15, 92, 98
Otter brown, 30, 67, 40
Outer Space, 191, 14, 30
Outrageous Orange, 12, 71, 100
Oxford Blue, 212, 100, 28
OU Crimson Red, 0, 100, 60
Pacific Blue, 191, 86, 79
Pakistan green, 120, 100, 40
Palatinate blue, 234, 83, 89
Palatinate purple, 308, 62, 41
Pale aqua, 206, 18, 90
Pale blue, 180, 26, 93
Pale brown, 30, 45, 60
Pale carmine, 5, 70, 69
Pale cerulean, 205, 31, 89
Pale chestnut, 358, 22, 87
Pale copper, 18, 53, 85
Pale cornflower blue, 210, 28, 94
Pale cyan, 200, 46, 97
Pale gold, 34, 40, 90
Pale goldenrod, 55, 29, 93
Pale green, 120, 39, 98
Pale lavender, 255, 18, 100
Pale magenta, 310, 47, 98
Pale magenta-pink, 330, 40, 100
Pale pink, 354, 13, 98
Pale plum, 300, 28, 87
Pale red-violet, 340, 49, 86
Pale robin egg blue, 169, 32, 87
Pale silver, 21, 7, 79
Pale spring bud, 59, 20, 93
Pale taupe, 25, 33, 74
Pale turquoise, 180, 26, 93
Pale violet, 270, 40, 100
Pale violet-red, 340, 49, 86
Pansy purple, 329, 80, 47
Paolo Veronese green, 168, 100, 61
Papaya whip, 37, 16, 100
Paradise pink, 347, 73, 90
Paris Green, 140, 60, 78
Pastel blue, 196, 16, 81
Pastel brown, 28, 37, 51
Pastel gray, 60, 5, 81
Pastel green, 120, 46, 87
Pastel magenta, 333, 37, 96
Pastel orange, 35, 72, 100
Pastel pink, 1, 26, 87
Pastel purple, 295, 13, 71
Pastel red, 3, 62, 100
Pastel violet, 302, 25, 80
Pastel yellow, 60, 41, 99
Patriarch, 300, 100, 50
Payne's grey, 206, 31, 47
Peach, 39, 29, 100
Peach, 26, 36, 100
Peach-orange, 30, 40, 100
Peach puff, 28, 27, 100
Peach-yellow, 39, 31, 98
Pear, 66, 78, 89
Pearl, 42, 15, 92
Pearl Aqua, 162, 37, 85
Pearly purple, 316, 43, 72
Peridot, 59, 100, 90
Periwinkle, 240, 20, 100
Persian blue, 229, 85, 73
Persian green, 173, 100, 65
Persian indigo, 258, 85, 48
Persian orange, 26, 59, 85
Persian pink, 329, 49, 97
Persian plum, 0, 75, 44
Persian red, 0, 75, 80
Persian rose, 326, 84, 100
Persimmon, 22, 100, 93
Peru, 30, 69, 80
Phlox, 292, 100, 100
Phthalo blue, 233, 100, 54
Phthalo green, 151, 66, 21
Picton blue, 200, 70, 91
Pictorial carmine, 338, 94, 76
Piggy pink, 343, 13, 99
Pine green, 175, 99, 47
Pineapple, 289, 35, 36
Pink, 350, 25, 100
Pink (Pantone), 328, 67, 84
Pink Flamingo, 300, 54, 99
Pink lace, 319, 13, 100
Pink lavender, 311, 18, 85
Pink-orange, 20, 60, 100
Pink pearl, 324, 26, 91
Pink raspberry, 339, 100, 60
Pink Sherbet, 346, 42, 97
Pistachio, 96, 42, 77
Platinum, 40, 1, 90
Plum, 307, 51, 56
Plum (web), 300, 28, 87
Pomp and Power, 290, 32, 56
Popstar, 350, 58, 75
Portland Orange, 11, 79, 100
Powder blue, 187, 23, 90
Princeton orange, 26, 85, 96
Prune, 0, 75, 44
Prussian blue, 205, 100, 33
Psychedelic purple, 292, 100, 100
Puce, 345, 33, 80
Puce red, 353, 59, 45
Pullman Brown (UPS Brown), 33, 77, 39
Pullman Green, 45, 36, 59
Pumpkin, 24, 91, 100
Purple (HTML), 300, 100, 50
Purple (Munsell), 288, 100, 77
Purple (X11), 277, 87, 94
Purple Heart, 270, 66, 61
Purple mountain majesty, 269, 34, 71
Purple navy, 236, 39, 50
Purple pizzazz, 312, 69, 100
Purple taupe, 311, 20, 31
Purpureus, 288, 55, 68
Quartz, 313, 11, 32
Queen blue, 211, 55, 58
Queen pink, 336, 12, 91
Quinacridone magenta, 338, 59, 56
Rackley, 204, 45, 66
Radical Red, 348, 79, 100
Raisin black, 300, 8, 14
Rajah, 29, 62, 98
Raspberry, 337, 95, 89
Raspberry glace, 343, 34, 57
Raspberry pink, 330, 65, 89
Raspberry rose, 338, 62, 70
Raw Sienna, 24, 58, 84
Raw umber, 33, 48, 51
Razzle dazzle rose, 315, 80, 100
Razzmatazz, 338, 84, 89
Razzmic Berry, 308, 45, 55
Rebecca Purple, 270, 67, 60
Red, 0, 100, 100
Red (Crayola), 347, 87, 93
Red (Munsell), 345, 100, 95
Red (NCS), 345, 99, 77
Red (Pantone), 355, 83, 93
Red (pigment), 358, 88, 93
Red (RYB), 5, 93, 100
Red-brown, 0, 75, 65
Red devil, 353, 99, 53
Red-orange, 3, 71, 100
Red-purple, 328, 100, 89
Red-violet, 322, 89, 78
Redwood, 6, 50, 64
Regalia, 267, 65, 50
Registration black, 0, 0, 0
Resolution blue, 224, 100, 53
Rhythm, 242, 21, 59
Rich black, 180, 100, 25
Rich black (FOGRA29), 207, 95, 8
Rich black (FOGRA39), 210, 67, 1
Rich brilliant lavender, 291, 34, 100
Rich carmine, 342, 100, 84
Rich electric blue, 199, 96, 82
Rich lavender, 276, 48, 81
Rich lilac, 284, 51, 82
Rich maroon, 338, 73, 69
Rifle green, 84, 26, 30
Roast coffee, 1, 42, 44
Robin egg blue, 180, 100, 80
Rocket metallic, 355, 8, 54
Roman silver, 221, 13, 59
Rose, 330, 100, 100
Rose bonbon, 330, 73, 98
Rose ebony, 4, 32, 40
Rose gold, 351, 40, 72
Rose madder, 355, 83, 89
Rose pink, 320, 60, 100
Rose quartz, 303, 11, 67
Rose red, 340, 85, 76
Rose taupe, 0, 35, 56
Rose vale, 357, 54, 67
Rosewood, 353, 100, 40
Rosso corsa, 0, 100, 83
Rosy brown, 0, 24, 74
Royal azure, 220, 100, 66
Royal blue, 219, 100, 40
Royal blue, 225, 71, 88
Royal fuchsia, 321, 78, 79
Royal purple, 267, 52, 66
Royal yellow, 48, 62, 98
Ruber, 339, 66, 81
Rubine red, 335, 100, 82
Ruby, 337, 92, 88
Ruby red, 354, 89, 61
Ruddy, 351, 100, 100
Ruddy brown, 25, 79, 73
Ruddy pink, 354, 37, 88
Rufous, 8, 96, 66
Russet, 26, 79, 50
Russian green, 120, 29, 57
Russian violet, 270, 70, 30
Rust, 18, 92, 72
Rusty red, 352, 80, 85
Sacramento State green, 164, 100, 34
Saddle brown, 25, 86, 55
Safety orange, 28, 100, 100
Safety orange (blaze orange), 24, 100, 100
Safety yellow, 53, 99, 93
Saffron, 45, 80, 96
Sage, 55, 27, 74
St. Patrick's blue, 236, 71, 48
Salmon, 6, 54, 98
Salmon pink, 350, 43, 100
Sand, 45, 34, 76
Sand dune, 43, 85, 59
Sandstorm, 52, 73, 93
Sandy brown, 28, 61, 96
Sandy taupe, 43, 85, 59
Sangria, 356, 100, 57
Sap green, 93, 66, 49
Sapphire, 216, 92, 73
Sapphire blue, 203, 100, 65
Satin sheen gold, 43, 74, 80
Scarlet, 8, 100, 100
Scarlet, 350, 94, 99
Schauss pink, 344, 43, 100
School bus yellow, 51, 100, 100
Screamin' Green, 122, 54, 100
Sea blue, 197, 100, 58
Sea green, 146, 67, 55
Seal brown, 0, 60, 20
Seashell, 25, 7, 100
Selective yellow, 44, 100, 100
Sepia, 30, 82, 44
Shadow, 37, 33, 54
Shadow blue, 214, 28, 65
Shampoo, 318, 19, 100
Shamrock green, 156, 100, 62
Sheen Green, 80, 100, 83
Shimmering Blush, 349, 38, 85
Shocking pink, 315, 94, 99
Shocking pink (Crayola), 300, 56, 100
Sienna, 12, 83, 53
Silver, 0, 0, 75
Silver chalice, 0, 0, 67
Silver Lake blue, 212, 50, 73
Silver pink, 3, 12, 77
Silver sand, 200, 2, 76
Sinopia, 17, 95, 80
Skobeloff, 180, 100, 45
Sky blue, 197, 43, 92
Sky magenta, 320, 45, 81
Slate blue, 248, 56, 80
Slate gray, 210, 22, 56
Smalt (Dark powder blue), 220, 100, 60
Smitten, 329, 68, 78
Smoke, 132, 12, 51
Smoky black, 30, 50, 6
Smoky Topaz, 357, 59, 58
Snow, 0, 2, 100
Soap, 249, 16, 94
Solid pink, 352, 59, 54
Sonic silver, 0, 0, 46
Spartan Crimson, 359, 88, 62
Space cadet, 226, 64, 32
Spanish bistre, 52, 61, 50
Spanish blue, 203, 100, 72
Spanish carmine, 340, 100, 82
Spanish crimson, 345, 89, 90
Spanish gray, 0, 0, 60
Spanish green, 153, 100, 57
Spanish orange, 25, 100, 91
Spanish pink, 1, 23, 97
Spanish red, 350, 100, 90
Spanish sky blue, 180, 100, 100
Spanish violet, 264, 69, 51
Spanish viridian, 163, 100, 50
Spicy mix, 17, 45, 55
Spiro Disco Ball, 195, 94, 99
Spring bud, 80, 100, 99
Spring green, 150, 100, 100
Star command blue, 200, 100, 72
Steel blue, 207, 61, 71
Steel pink, 300, 75, 80
Stil de grain yellow, 48, 62, 98
Stizza, 0, 100, 60
Stormcloud, 189, 25, 42
Straw, 54, 51, 89
Strawberry, 341, 64, 99
Sunglow, 45, 80, 100
Sunray, 36, 62, 89
Sunset, 35, 34, 98
Sunset orange, 4, 67, 99
Super pink, 323, 48, 81
Tan, 34, 33, 82
Tangelo, 19, 100, 98
Tangerine, 33, 100, 95
Tangerine yellow, 48, 100, 100
Tango pink, 355, 50, 89
Taupe, 27, 31, 28
Taupe gray, 320, 4, 55
Tea green, 100, 20, 94
Tea rose, 5, 51, 97
Tea rose, 0, 20, 96
Teal, 180, 100, 50
Teal blue, 194, 60, 53
Teal deer, 140, 33, 90
Teal green, 179, 100, 51
Telemagenta, 334, 75, 81
Tenné, 25, 100, 80
Terra cotta, 10, 60, 89
Thistle, 300, 12, 85
Thulian pink, 333, 50, 87
Tickle Me Pink, 342, 46, 99
Tiffany Blue, 178, 95, 73
Tiger's eye, 30, 73, 88
Timberwolf, 33, 4, 86
Titanium yellow, 58, 100, 93
Tomato, 9, 72, 100
Toolbox, 246, 44, 75
Topaz, 35, 51, 100
Tractor red, 350, 94, 99
Trolley Grey, 0, 0, 50
Tropical rain forest, 168, 100, 46
Tropical violet, 282, 26, 87
True Blue, 207, 100, 81
Tufts Blue, 212, 66, 76
Tulip, 357, 47, 100
Tumbleweed, 24, 39, 87
Turkish rose, 347, 37, 71
Turquoise, 174, 71, 88
Turquoise blue, 176, 100, 100
Turquoise green, 142, 25, 84
Tuscan, 35, 34, 98
Tuscan brown, 25, 50, 44
Tuscan red, 0, 42, 49
Tuscan tan, 26, 45, 65
Tuscany, 0, 20, 75
Twilight lavender, 329, 47, 54
Tyrian purple, 325, 98, 40
UA blue, 222, 100, 67
UA red, 339, 100, 85
Ube, 253, 38, 76
UCLA Blue, 221, 44, 58
UCLA Gold, 42, 100, 100
UFO Green, 141, 71, 82
Ultramarine, 244, 93, 56
Ultramarine blue, 228, 73, 96
Ultra pink, 300, 56, 100
Ultra red, 350, 57, 99
Umber, 21, 28, 39
Unbleached silk, 22, 21, 100
United Nations blue, 216, 60, 90
University of California Gold, 40, 79, 72
Unmellow yellow, 60, 60, 100
UP Forest green, 149, 99, 27
UP Maroon, 359, 86, 48
Upsdell red, 356, 82, 68
Urobilin, 44, 85, 88
USAFA blue, 209, 100, 60
USC Cardinal, 0, 100, 60
USC Gold, 48, 100, 100
University of Tennessee Orange, 31, 100, 97
Utah Crimson, 342, 100, 83
Vanilla, 48, 30, 95
Vanilla ice, 344, 41, 95
Vegas gold, 50, 55, 77
Venetian red, 356, 96, 78
Verdigris, 177, 63, 70
Vermilion, 5, 77, 89
Vermilion, 8, 86, 85
Veronica, 277, 87, 94
Very light azure, 208, 54, 98
Very light blue, 240, 60, 100
Very light malachite green, 135, 57, 91
Very light tangelo, 25, 53, 100
Very pale orange, 30, 25, 100
Very pale yellow, 60, 25, 100
Violet, 274, 100, 100
Violet (color wheel), 270, 100, 100
Violet (RYB), 286, 99, 69
Violet (web), 300, 45, 93
Violet-blue, 229, 72, 70
Violet-red, 336, 66, 97
Viridian, 161, 51, 51
Viridian green, 181, 100, 60
Vista blue, 218, 43, 85
Vivid amber, 45, 100, 80
Vivid auburn, 2, 75, 57
Vivid burgundy, 349, 82, 62
Vivid cerise, 328, 87, 85
Vivid cerulean, 197, 100, 93
Vivid crimson, 345, 100, 80
Vivid gamboge, 36, 100, 100
Vivid lime green, 74, 96, 84
Vivid malachite, 135, 100, 80
Vivid mulberry, 288, 95, 89
Vivid orange, 22, 100, 100
Vivid orange peel, 18, 100, 100
Vivid orchid, 288, 100, 100
Vivid raspberry, 335, 100, 100
Vivid red, 357, 95, 97
Vivid red-tangelo, 20, 84, 87
Vivid sky blue, 192, 100, 100
Vivid tangelo, 23, 84, 94
Vivid tangerine, 12, 46, 100
Vivid vermilion, 19, 84, 90
Vivid violet, 277, 100, 100
Vivid yellow, 53, 99, 100
Volt, 72, 100, 100
Warm black, 180, 100, 25
Waterspout, 184, 34, 98
Wenge, 7, 18, 39
Wheat, 39, 27, 96
White, 0, 0, 100
White smoke, 0, 0, 96
Wild blue yonder, 226, 22, 82
Wild orchid, 330, 47, 83
Wild Strawberry, 329, 74, 100
Wild watermelon, 350, 57, 99
Willpower orange, 21, 100, 99
Windsor tan, 30, 99, 65
Wine, 353, 59, 45
Wine dregs, 336, 52, 40
Wisteria, 281, 27, 86
Wood brown, 33, 45, 76
Xanadu, 136, 14, 53
Yale Blue, 212, 90, 57
Yankees blue, 221, 57, 25
Yellow, 60, 100, 100
Yellow (Crayola), 50, 48, 99
Yellow (Munsell), 51, 100, 94
Yellow (NCS), 50, 100, 100
Yellow (Pantone), 53, 100, 100
Yellow (process), 56, 100, 100
Yellow (RYB), 60, 80, 100
Yellow-green, 80, 76, 80
Yellow Orange, 34, 74, 100
Yellow rose, 56, 100, 100
Zaffre, 233, 100, 66
Zinnwaldite brown, 23, 82, 17
Zomp, 166, 66, 65