package com.example.faytxzen.colordoctor.fragments;

import android.app.Fragment;
import android.graphics.drawable.GradientDrawable;
import android.widget.ListView;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.generators.HSVGradients;

/**
 * Created by faytxzen on 4/20/15.
 */
public abstract class BaseHSVFragment extends Fragment {
    protected final int NUM_ITEMS = MainActivity.NUM_ITEMS;
    protected ListView mListView;
    protected TextView mHues, mSaturation, mValue;

    protected abstract float[] getHues();
    protected abstract float getSaturation();

    protected void restoreFields(String hues, String saturation, String value){
        mHues.setText(hues);
        mSaturation.setText(saturation);
        mValue.setText(value);
    }

    protected GradientDrawable[] getValues(int state){
        switch(state){
            case 0: //hues
                return HSVGradients.getHSV(NUM_ITEMS);

            case 1: //saturation
                float[] hues = getHues();
                return HSVGradients.getHSV(hues[0], hues[1], NUM_ITEMS);

            case 2: //values
                hues = getHues();
                return HSVGradients.getHSV(hues[0], hues[1], getSaturation(), NUM_ITEMS);

            default:
                throw new IllegalArgumentException();
        }
    }
}
