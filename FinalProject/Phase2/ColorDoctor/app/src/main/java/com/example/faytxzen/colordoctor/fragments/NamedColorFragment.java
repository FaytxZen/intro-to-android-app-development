package com.example.faytxzen.colordoctor.fragments;

import android.app.Fragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;
import com.example.faytxzen.colordoctor.adapters.HSVAdapter;
import com.example.faytxzen.colordoctor.contentproviders.ColorProvider;
import com.example.faytxzen.colordoctor.database.ColorHelper;
import com.example.faytxzen.colordoctor.generators.HSVGradients;

import java.util.ArrayList;
import java.util.Arrays;

public class NamedColorFragment extends Fragment {
    private final boolean DEBUG = MainActivity.DEBUG;
    private final String LIST_TAG = "LIST_TAG";
    private String[] params;
    private ColorRecord[] colorRecords;
    private ListView mListView;

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray(LIST_TAG, params);
        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_named_listview, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if(savedInstanceState != null){
            params = savedInstanceState.getStringArray(LIST_TAG);
        }
        else {
            MainActivity.FragmentMessage msg = ((MainActivity) getActivity()).getFragmentMessage();
            if(msg != null){
                params = msg.getMsg();
            }
        }

        float[] hues = getHues(params[0]);
        float saturation = Float.parseFloat(params[1]);
        float value = Float.parseFloat(params[2]);
        float delta = HSVGradients.getDelta(MainActivity.NUM_ITEMS);

        GradientDrawable[] gradients = getGradients(getColorRecords(getColors(hues[0], hues[1], saturation, value, delta)));
        if(gradients.length == 0)
            Toast.makeText(getActivity().getApplicationContext(), "No results were found!", Toast.LENGTH_SHORT).show();

        mListView = (ListView) getView().findViewById(R.id.swatch_list);
        mListView.setAdapter(new HSVAdapter(getActivity(),gradients));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ColorRecord record = colorRecords[position];
                String info = String.format("Name: %s, Hue: %s, Saturation: %s, Value: %s", record.name, record.hue, record.saturation, record.value);
                Toast.makeText(getActivity().getApplicationContext(), info, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private float[] getHues(String hues){
        String[] pair = hues.split(",");

        float[] result = new float[pair.length];

        for(int i = 0; i < pair.length; i++){
            result[i] = Float.parseFloat(pair[i]);
        }

        return result;
    }

    private GradientDrawable[] getGradients(ColorRecord[] records){
        if(DEBUG) Log.d("NamedColorFragment", "getGradients(ColorRecord[] records)");
        GradientDrawable[] gradients = new GradientDrawable[records.length];
        for(int i = 0; i < records.length; i++){

            int color = Color.HSVToColor(new float[]{records[i].hue, records[i].saturation, records[i].value });
            gradients[i] = new GradientDrawable( GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ color, color } );
        }
        return gradients;
    }

    private ColorRecord[] getColorRecords(Cursor cursor){
        if(DEBUG) Log.d("NamedColorFragment", "getColorRecords(Cursor cursor)");

        ArrayList<ColorRecord> results = new ArrayList<>();

        cursor.moveToFirst();
        while(!cursor.isAfterLast()){
            results.add(new ColorRecord(
                    cursor.getString(0),
                    cursor.getFloat(1),
                    cursor.getFloat(2),
                    cursor.getFloat(3))
            );
            cursor.moveToNext();
        }
        cursor.close();

        return (colorRecords = results.toArray(new ColorRecord[results.size()]));
    }

    private Cursor getColors(float startHue, float endHue, float saturation, float value, float delta){
        ContentResolver resolver = getActivity().getContentResolver();

        String[] projection = {
                ColorHelper.COLOR_NAME_COLUMN,
                ColorHelper.HUE_COLUMN,
                ColorHelper.SATURATION_COLUMN,
                ColorHelper.VALUE_COLUMN
        };

        String selection = String.format("(%s BETWEEN ? AND ?) AND (%s BETWEEN ? AND ?) AND (%s BETWEEN ? AND ?)",
                projection[1], projection[2], projection[3]);
        String[] selectionArgs = { startHue+"", endHue+"", (saturation-delta)+"", (saturation+delta)+"", (value-delta)+"", (value+delta)+""};

        return resolver.query(ColorProvider.CONTENT_URI, projection, selection, selectionArgs, null, null );
    }

    private class ColorRecord {
        String name;
        float hue, saturation, value;

        public ColorRecord(String name, float hue, float saturation, float value){
            this.name = name;
            this.hue = hue;
            this.saturation = saturation;
            this.value = value;
        }

        public String toString(){
            return String.format("{ name: %s, hue: %s, saturation: %s, value: %s}", name, hue, saturation, value);
        }
    }
}
