package com.example.faytxzen.colordoctor.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.faytxzen.colordoctor.MainActivity;
import com.example.faytxzen.colordoctor.R;
import com.example.faytxzen.colordoctor.adapters.HSVAdapter;
import com.example.faytxzen.colordoctor.generators.HSVGradients;

/**
 * Created by faytxzen on 4/20/15.
 */
public class SaturationFragment extends BaseHSVFragment {
    /* DATA MEMBERS
     *=============================*/
    private final int STATE = 1;
    private final boolean DEBUG = MainActivity.DEBUG;
    private final int NUM_ITEMS = MainActivity.NUM_ITEMS;
    private final String LIST_TAG = "LIST_TAG";

    /* LIFECYCLE METHODS
     *=============================*/
    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putStringArray(LIST_TAG, new String[]{
                mHues.getText().toString(),
                mSaturation.getText().toString(),
                mValue.getText().toString()
        });

        super.onSaveInstanceState(outState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_hsv_listview, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mHues = (TextView) getView().findViewById(R.id.hues_value);
        mSaturation = (TextView) getView().findViewById(R.id.saturation_value);
        mValue = (TextView) getView().findViewById(R.id.values_value);

        if(savedInstanceState != null){
            String[] states = savedInstanceState.getStringArray(LIST_TAG);
            restoreFields(states[0], states[1], states[2]);
        }
        else {
            MainActivity.FragmentMessage msg = ((MainActivity) getActivity()).getFragmentMessage();
            if(msg != null){
                mHues.setText(msg.getMsg()[0]);
            }
        }

        mListView = (ListView) getView().findViewById(R.id.swatch_list);
        mListView.setAdapter(new HSVAdapter(getActivity(), getValues(STATE)));
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                setSaturation(HSVGradients.getSaturationOrValue(NUM_ITEMS, position));

                MainActivity.FragmentMessage msg = new MainActivity.FragmentMessage(
                        new String[]{mHues.getText().toString(), mSaturation.getText().toString()}
                );
                ((MainActivity) getActivity()).startFragment(new ValueFragment(), msg);
            }
        });
    }

    /* METHODS
     *=============================*/
    @Override
    protected float[] getHues(){
        String[] hueStr = mHues.getText().toString().split(",");
        float startHue = Float.parseFloat(hueStr[0]);
        float endHue = Float.parseFloat(hueStr[1]);
        return new float[]{ startHue, endHue };
    }

    @Override
    protected float getSaturation(){
        return Float.parseFloat(mSaturation.getText().toString());
    }

    private void setSaturation(float saturation){
        mSaturation.setText(saturation + "");
    }
}