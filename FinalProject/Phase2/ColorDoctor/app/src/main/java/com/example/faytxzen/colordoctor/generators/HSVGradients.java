package com.example.faytxzen.colordoctor.generators;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;

public class HSVGradients {
    private HSVGradients(){}

    /* METHODS
     *=============================*/
    /**
     *
     * @param numChoices
     * @param position
     * @return
     */
    public static float[] getStartEndHues(int numChoices, int position){
        float colorSteps = 360f/(numChoices);
        return new float[] {
                colorSteps * position,
                colorSteps * ++position
        };
    }

    public static float getSaturationOrValue(int numChoices, int position){
        float colorSteps = 1f/numChoices;
        return colorSteps * position;
    }

    public static float getDelta(int numChoices){
        return 1f/numChoices;
    }

    /**
     * Returns an array of gradients with varying hues
     * @param numChoices
     * @return
     */
    public static GradientDrawable[] getHSV(int numChoices){
        GradientDrawable[] result = new GradientDrawable[numChoices];

        float colorSteps = 360f/(numChoices);

        for(int i = 0, j = 0 ; i < numChoices && j < numChoices; j++, i++){
            int leftColor = Color.HSVToColor(new float[]{colorSteps * i, 1f, 1f});
            int rightColor = Color.HSVToColor(new float[]{colorSteps * (i+1), 1f, 1f});
            result[j] = new GradientDrawable( GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ leftColor, rightColor });
        }

        return result;
    }

    /**
     * Returns an array of gradients with varying saturations
     * @param startHue
     * @param endHue
     * @param numChoices
     * @return
     */
    public static GradientDrawable[] getHSV(float startHue, float endHue, int numChoices){
        GradientDrawable[] result = new GradientDrawable[numChoices];

        float colorSteps = 1f/numChoices;

        for(int i = 0 ; i < numChoices ; i++){
            float saturation = colorSteps * i;
            int leftColor = Color.HSVToColor(new float[]{ startHue, saturation, 1f });
            int rightColor = Color.HSVToColor(new float[]{ endHue, saturation, 1f });

            result[i] = new GradientDrawable( GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ leftColor, rightColor } );
        }

        return result;
    }

    /**
     * Returns an array of gradients with varying values
     * @param startHue
     * @param endHue
     * @param saturation
     * @param numChoices
     * @return
     */
    public static GradientDrawable[] getHSV(float startHue, float endHue, float saturation, int numChoices){
        GradientDrawable[] result = new GradientDrawable[numChoices];

        float colorSteps = 1f/numChoices;

        for(int i = 0; i < numChoices; i++){
            float value = colorSteps * i;
            int leftColor = Color.HSVToColor(new float[]{ startHue, saturation, value });
            int rightColor = Color.HSVToColor(new float[]{ endHue, saturation, value });

            result[i] = new GradientDrawable(  GradientDrawable.Orientation.LEFT_RIGHT, new int[]{ leftColor, rightColor } );
        }

        return result;
    }
}
